module task9 {
    exports service;
    provides service.GreeterService with task9.imp.FrenchGreeter, task9.imp.GermanGreeter;
}