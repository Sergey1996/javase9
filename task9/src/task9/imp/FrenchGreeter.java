package task9.imp;

import service.GreeterService;

import java.util.Locale;

public class FrenchGreeter implements GreeterService {
    public String greet(String subject) {
        return "Bonjour " + subject;
    }

    public Locale getLocale() {
        return Locale.FRENCH;
    }
}
