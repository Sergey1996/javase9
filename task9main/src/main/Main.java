package main;

import service.GreeterService;

import java.util.ServiceLoader;

public class Main {
    public static void main(String[] args) {
        ServiceLoader<GreeterService> services = ServiceLoader.load(GreeterService.class);
        GreeterService greeterService;
        for(GreeterService greeterService1 : services){
            System.out.println(greeterService1.getClass());
        }
    }
}
