package chapteres.chap09.task12;

public class App {
    public static void main(String[] args) {
        String str = "25.12.201801.01.201907.01.2019";
        String result = str.replaceAll("(?<day>\\d{2}).(?<month>\\d{2}).(?<year>\\d{4})",
                "day: ${day}, month: ${month}, year: ${year}; \n");
        System.out.println(result);
    }
}
