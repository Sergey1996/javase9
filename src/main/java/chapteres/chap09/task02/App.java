package chapteres.chap09.task02;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public class App {
    public static void main(String[] args) throws IOException {
        String fileName = "reader";
        File read = new File(fileName);
        File write = new File(fileName + ".toc");
        Scanner scan = new Scanner(read);
        Map<String, HashSet<Integer>> map = new HashMap<>();
        int i = 1;
        while(scan.hasNext()) {
            String str = scan.nextLine();
            String [] masStr = str.split("\\s+");
            for(String s : masStr) {
                if(map.containsKey(s)) {
                    map.get(s).add(i);
                } else {
                    HashSet<Integer> set = new HashSet<>();
                    set.add(i);
                    map.put(s,set);
                }
            }
            i++;
        }
        FileWriter fileWriter = new FileWriter(write);
        for (String str : map.keySet()) {
            fileWriter.write(str + " : " + map.get(str) + "\n");
        }
        fileWriter.close();
    }
}
