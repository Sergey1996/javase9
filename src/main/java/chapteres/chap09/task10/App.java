package chapteres.chap09.task10;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class App {
    public static void main(String[] args) {

        String str = "-1++6--5+6+-954+++-";
        //find()
        Pattern pattern = Pattern.compile("[+-]?\\d+");
        Matcher matcher = pattern.matcher(str);
        ArrayList<Integer> list = new ArrayList<>();
        while (matcher.find()) {
            String s = matcher.group();
            list.add(Integer.parseInt(s));
        }
        System.out.println(list);

        //split()
        Pattern pattern1 = Pattern.compile("\\+|-");
        ArrayList<Integer> list2 = (ArrayList<Integer>) Stream.of(pattern1.split(str)).filter(s -> s.length()>0)
                .map(s -> Integer.parseInt(s)).collect(Collectors.toList());
        System.out.println(list2);
    }
}
