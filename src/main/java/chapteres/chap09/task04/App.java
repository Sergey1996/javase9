package chapteres.chap09.task04;

import java.io.*;
import java.util.Scanner;

public class App {

    public static int getNumberScan(File file) throws FileNotFoundException {
        int num = 0;
        Scanner scanner = new Scanner(file);
        while(scanner.hasNext()) {
            scanner.nextLine();
            num++;
        }
        scanner.close();
        return num;
    }

    public static int getNumberBuf(File file) throws IOException {
        BufferedReader buff = new BufferedReader(new FileReader(file));
        int num = 0;
        while(buff.read() != -1) {
             buff.readLine();
             num++;
        }
        buff.close();
        return num;
    }

    public static int getNumberBuf2(File file) throws IOException {
        BufferedReader buff = new BufferedReader(new FileReader(file));
        int num = 0;
        num = (int) buff.lines().count();
        buff.close();
        return num;
    }

    public static void main(String[] args) throws IOException {
        File file = new File("WarAndPeace.txt");

        long start = System.currentTimeMillis();
        int num = getNumberScan(file);
        long finish = System.currentTimeMillis();
        System.out.println(num + " time : " + (finish-start));

        start = System.currentTimeMillis();
        num = getNumberBuf(file);
        finish = System.currentTimeMillis();
        System.out.println(num + " time : " + (finish-start));

        start = System.currentTimeMillis();
        num = getNumberBuf2(file);
        finish = System.currentTimeMillis();
        System.out.println(num + " time : " + (finish-start));

    }
}
