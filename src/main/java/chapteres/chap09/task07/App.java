package chapteres.chap09.task07;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public class App {

    public static void main(String[] args) throws NoSuchAlgorithmException, IOException {

        MessageDigest messageDigest = MessageDigest.getInstance("SHA-512");
        messageDigest.update(Files.readAllBytes(Paths.get("reader")));
        System.out.println(new String(messageDigest.digest()));
    }
}
