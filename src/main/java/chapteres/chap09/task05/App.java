package chapteres.chap09.task05;

import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

public class App {
    public static void main(String[] args) {
        Charset ch = Charset.forName(StandardCharsets.US_ASCII.name());
        CharsetEncoder charsetEncoder = ch.newEncoder();
        System.out.println(Arrays.toString(charsetEncoder.replacement()));
    }
}
