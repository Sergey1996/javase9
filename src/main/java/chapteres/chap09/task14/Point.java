package chapteres.chap09.task14;

import java.io.Serializable;

public class Point implements Serializable {

    public double x;
    public double y;

    public Point() {}

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }
}
