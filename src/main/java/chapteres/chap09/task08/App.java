package chapteres.chap09.task08;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.*;
import java.util.Collections;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class App {
    public static void main(String[] args) throws URISyntaxException, IOException {
        Path zipPath = Paths.get("myfile.zip");
        try (ZipOutputStream zipOut = new ZipOutputStream(new FileOutputStream(zipPath.toFile()))) {
            try (Stream<Path> paths = Files.walk(Paths.get("D://Users"))) {
                for (Path path : paths.collect(Collectors.toList())) {
                    if(path.toFile().isFile()) {
                        System.out.println(path.toFile());
                        try (FileInputStream in = new FileInputStream(path.toFile())) {
                            ZipEntry entry = new ZipEntry(path.toFile().getName());
                            zipOut.putNextEntry(entry);
                            byte[] buf = new byte[in.available()];
                            in.read(buf);
                            zipOut.write(buf);
                        }
                    }
                }
            }
        }

    }
}
