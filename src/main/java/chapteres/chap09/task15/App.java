package chapteres.chap09.task15;


import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

public class App {

    public static void write(String file, Point...point) {
        try(ObjectOutputStream objOut = new ObjectOutputStream(Files.newOutputStream(Paths.get(file)))) {
            objOut.writeObject(point);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Point[] read(File file) {
        Point[] points = null;
        try(ObjectInputStream objOut = new ObjectInputStream(Files.newInputStream(file.toPath()))) {
            points = (Point[]) objOut.readObject();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return points;
    }

    public static void main(String[] args) {
        /*Point [] points = {new Point(1,2),new Point(2,3),new Point(3,4)};
        write("points2", points);*/

        for(Point point : read(new File("points2"))) {
            //System.out.println(point.x + "," + point.y);
            System.out.println(Arrays.toString(point.points));
        }
    }
}
