package chapteres.chap09.task13;

import java.io.*;

public class App {

    public static<T extends Serializable> T clone(T t) {
        T res = null;
        try(ByteArrayOutputStream arr = new ByteArrayOutputStream()) {
            try(ObjectOutputStream objOut = new ObjectOutputStream(arr)) {
                objOut.writeObject(t);
                try (ObjectInputStream objIn = new ObjectInputStream(new ByteArrayInputStream(arr.toByteArray()))) {
                    res = (T) objIn.readObject();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return res;
    }

    public static void main(String[] args) {

    }
}
