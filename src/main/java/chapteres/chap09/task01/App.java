package chapteres.chap09.task01;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;


public class App {

    public static void method(InputStream in, OutputStream out) throws IOException {
        int c = in.read();
        while (c != -1){
           out.write(c);
           c = in.read();
        }

    }

    public static void method2(InputStream in, OutputStream out) throws IOException {
        Files.copy(in, Paths.get("out"), StandardCopyOption.REPLACE_EXISTING);
        Files.copy(Paths.get("out"),out);
    }



    public static void main(String[] args) throws IOException {
        byte [] bytes = " My test str for JavaSE9".getBytes();
        InputStream in = new ByteArrayInputStream(bytes);
        OutputStream out = new ByteArrayOutputStream();
        method(in,out);
        System.out.println(new String(((ByteArrayOutputStream) out).toByteArray()));
        in.close();
        out.close();
    }
}
