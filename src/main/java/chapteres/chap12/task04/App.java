package chapteres.chap12.task04;

import java.time.LocalDate;
import java.time.Month;

public class App {
    public static void main(String[] args) {

        int month = 12;//Integer.parseInt(args[0]);
        int year = 2018;//Integer.parseInt(args[1]);
        LocalDate date = LocalDate.of(year,month,1);

        System.out.println("Mn  Ts  Wn  Th  Fr  St  Sn");
        int flag = 0;
        String str = "";
        LocalDate val;
        while (date.getMonth().equals(Month.of(month))) {
            if(date.getDayOfMonth() == 1) {
                switch (date.getDayOfWeek()) {
                    case MONDAY:
                        str = date.getDayOfMonth() + "   ";
                        flag = 1;
                        break;
                    case TUESDAY:
                        str = "    " + date.getDayOfMonth() + "   ";
                        flag = 2;
                        break;
                    case WEDNESDAY:
                        str = "        " + date.getDayOfMonth() + "   ";
                        flag = 3;
                        break;
                    case THURSDAY:
                        str = "            " + date.getDayOfMonth() + "   ";
                        flag = 4;
                        break;
                    case FRIDAY:
                        str = "                " + date.getDayOfMonth() + "   ";
                        flag = 5;
                        break;
                    case SATURDAY:
                        str = "                    " + date.getDayOfMonth() + "   ";
                        flag = 6;
                        break;
                    case SUNDAY:
                        str = "                        " + date.getDayOfMonth() + "   ";
                        flag = 7;
                        break;
                }

            } else {
                if(date.getDayOfMonth()<10) {
                    str += date.getDayOfMonth() + "   ";
                } else {
                    str += date.getDayOfMonth() + "  ";
                }
                flag++;
            }
            val = date.plusDays(1);
            if (flag == 7 || val.getMonth().getValue() != month) {
                System.out.println(str);
                flag = 0;
                str = "";
            }
            date = date.plusDays(1);
        }
    }
}
