package chapteres.chap12.task12;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class App {

    private static boolean check(ZonedDateTime time) {
        LocalDateTime timeOffSet = LocalDateTime.now().plusSeconds(time.getOffset().getTotalSeconds());
        if(time.getYear() == timeOffSet.getYear() && time.getMonth().getValue() == timeOffSet.getMonth().getValue()
                && time.getDayOfMonth() == timeOffSet.getDayOfMonth()
                && (time.getHour() == timeOffSet.getHour()+1 || time.getHour() == timeOffSet.getHour())) {
            return true;
        } return false;
    }

    public static void main(String[] args) {
        boolean bool = check(ZonedDateTime.of(2018,12,14,11,30,0,0,
                ZoneId.of("Australia/Lindeman")));
        System.out.println(bool);
        System.out.println(LocalDateTime.now());
        System.out.println(ZoneId.of("Australia/Lindeman").getRules().getOffset(LocalDateTime.now()));
    }
}
