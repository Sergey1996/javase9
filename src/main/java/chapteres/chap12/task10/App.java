package chapteres.chap12.task10;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class App {

    public static LocalDateTime getZoneTime(ZoneId zone1, ZoneId zone2, LocalDateTime local, LocalTime time) {
        LocalDateTime time1 = local.plusHours(time.getHour()).plusMinutes(time.getMinute());
        ZonedDateTime zoned = ZonedDateTime.of(local.getYear(),local.getMonth().getValue(),local.getDayOfMonth(),local.getHour(),
                local.getMinute(),0,0,zone1);
        LocalDateTime res = time1.plusSeconds(zoned.getOffset().getTotalSeconds());
        res = res.plusSeconds(zone2.getRules().getOffset(local).getTotalSeconds());
        return res;
    }

    public static void main(String[] args) {
        System.out.println(getZoneTime(ZoneId.of("Europe/London"),ZoneId.of("America/Los_Angeles"),
                LocalDateTime.of(2018,12,31,22,0,0),
                LocalTime.of(10,30)));
    }
}
