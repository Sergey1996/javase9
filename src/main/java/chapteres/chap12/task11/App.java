package chapteres.chap12.task11;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZoneOffset;

public class App {

    private static LocalTime getTime(ZoneId zone1, ZoneId zone2, LocalDateTime time1, LocalDateTime time2) {
        ZoneOffset offset1 = zone1.getRules().getOffset(time1);
        ZoneOffset offset2 = zone2.getRules().getOffset(time2);
        LocalDateTime local1 = time1.plusSeconds(offset1.getTotalSeconds());
        LocalDateTime local2 = time2.plusSeconds(offset2.getTotalSeconds());
        LocalDateTime time = local2.minusMinutes(local1.getMinute());
        return LocalTime.of(time.getHour(),time.getMinute());

    }

    public static void main(String[] args) {
        System.out.println(getTime(ZoneId.of("Europe/London"),ZoneId.of("America/Los_Angeles"),
                LocalDateTime.of(2018,12,15,15,5),
                LocalDateTime.of(2018,12,15,16,40)));
    }
}
