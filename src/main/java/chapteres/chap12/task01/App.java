package chapteres.chap12.task01;

import java.time.LocalDate;
import java.time.Period;

public class App {
    public static void main(String[] args) {
        LocalDate programDay = LocalDate.of(2019,1,1).plus(Period.ofDays(255));
        System.out.println(programDay);
    }
}
