package chapteres.chap12.task03;

import java.time.LocalDate;
import java.time.temporal.Temporal;
import java.time.temporal.TemporalAdjuster;
import java.util.function.Predicate;

public class App {

    private static TemporalAdjuster next(Predicate<LocalDate> predicate) {
        return new TemporalAdjuster() {
            @Override
            public Temporal adjustInto(Temporal temporal) {
                LocalDate rezult = (LocalDate) temporal;
                rezult = rezult.plusDays(1);
                if (!predicate.test(rezult)) {
                    rezult = rezult.plusDays(1);
                    if (!predicate.test(rezult))
                        rezult = rezult.plusDays(1);
                }
                return rezult;
            }
        };
    }

    public static void main(String[] args) {
        LocalDate today = LocalDate.now();
        LocalDate day = today.with(next(w -> w.getDayOfWeek().getValue() < 6));
        System.out.println(day);

        LocalDate today1 = LocalDate.of(2018, 12, 14);
        LocalDate day1 = today1.with(next(w -> w.getDayOfWeek().getValue() < 6));
        System.out.println(day1);

        LocalDate today2 = LocalDate.of(2018, 12, 15);
        LocalDate day2 = today2.with(next(w -> w.getDayOfWeek().getValue() < 6));
        System.out.println(day2);

        LocalDate today3 = LocalDate.of(2018, 12, 16);
        LocalDate day3 = today3.with(next(w -> w.getDayOfWeek().getValue() < 6));
        System.out.println(day3);

        LocalDate today4 = LocalDate.of(2018, 12, 17);
        LocalDate day4 = today4.with(next(w -> w.getDayOfWeek().getValue() < 6));
        System.out.println(day4);
    }
}
