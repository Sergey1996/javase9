package chapteres.chap12.task02;

import java.time.LocalDate;

public class App {
    public static void main(String[] args) {
        LocalDate date1 = LocalDate.of(2000,2,29).plusYears(1);
        System.out.println(date1);
        LocalDate date2 = LocalDate.of(2000,2,29).plusYears(4);
        System.out.println(date2);
        LocalDate date3 = LocalDate.of(2000,2,29).plusYears(1).plusYears(1)
                .plusYears(1).plusYears(1);
        System.out.println(date3);
    }
}
