package chapteres.chap12.task09;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.function.Consumer;

public class App {
    public static void main(String[] args) {
        Object[] ob = ZoneId.getAvailableZoneIds().stream().peek(new Consumer<String>() {
            @Override
            public void accept(String s) {
                ZonedDateTime date = ZonedDateTime
                        .of(LocalDateTime.now().getYear(), LocalDateTime.now().getMonth().getValue(),
                                LocalDateTime.now().getDayOfMonth(), LocalDateTime.now().getHour(), LocalDateTime.now()
                                        .getMinute(), 0, 0, ZoneId.of(s));
                if(date.getOffset().getTotalSeconds() % 3600 != 0) {
                    System.out.println(date.getOffset() + "[" + s + "]");
                }
            }
        }).toArray();
    }
}
