package chapteres.chap12.task06;

import java.time.LocalDate;
import java.util.stream.Stream;

public class App {
    public static void main(String[] args) {

        LocalDate date = LocalDate.of(1900,1,1);
        while (date.getYear() < 2000) {
            if(date.getDayOfWeek().getValue() == 5 && date.getDayOfMonth() == 13) {
                System.out.println(date);
            }
            date = date.plusDays(1);
        }

        Object[] ob = Stream.iterate(LocalDate.of(1900, 1, 1), d -> d.plusDays(1))
                .limit(365 * 100 + 50).filter(d -> d.getDayOfWeek().getValue() == 5 && d.getDayOfMonth() == 13)
                .peek(d -> System.out.println(d)).toArray();
    }
}
