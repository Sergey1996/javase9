package chapteres.chap12.task07;

import java.time.LocalDateTime;

public class TimeInterval {

    LocalDateTime start;
    LocalDateTime finish;

    public boolean check(LocalDateTime start1, LocalDateTime finish1) {
        if (this.finish.compareTo(start1) <= 0) {
            return true;
        }
        if (this.start.compareTo(finish1) >= 0) {
            return true;
        }
        return false;
    }
}
