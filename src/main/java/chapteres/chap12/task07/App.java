package chapteres.chap12.task07;

import java.time.LocalDateTime;

public class App {
    public static void main(String[] args) {
        TimeInterval interval = new TimeInterval();
        interval.start = LocalDateTime.of(2018,12,11,12,00);
        interval.finish = LocalDateTime.of(2018,12,11,13,00);

        boolean res1 = interval.check(LocalDateTime.of(2018,12,11,12,30),
                LocalDateTime.of(2018,12,11,14,00));

        System.out.println(res1);

        boolean res2 = interval.check(LocalDateTime.of(2018,12,11,10,30),
                LocalDateTime.of(2018,12,11,12,30));

        System.out.println(res2);

        boolean res3 = interval.check(LocalDateTime.of(2018,12,11,14,00),
                LocalDateTime.of(2018,12,11,16,00));

        System.out.println(res3);
    }
}
