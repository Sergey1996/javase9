package chapteres.chap12.task08;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class App {
    public static void main(String[] args) {
        Object[] ob = ZoneId.getAvailableZoneIds().stream().peek(s -> System.out.println(ZonedDateTime.of(LocalDateTime.now().getYear(),
                LocalDateTime.now().getMonth().getValue(), LocalDateTime.now().getDayOfMonth(),
                LocalDateTime.now().getHour(), LocalDateTime.now().getMinute(), 0, 0, ZoneId.of(s))
                .getOffset() + "[" + s + "]")).toArray();
    }
}
