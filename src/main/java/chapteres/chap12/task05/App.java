package chapteres.chap12.task05;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class App {
    public static void main(String[] args) {

        LocalDate birthday = LocalDate.of(1996,05,26);
        LocalDate current = LocalDate.now();

        System.out.println(birthday.until(current, ChronoUnit.DAYS));
    }
}
