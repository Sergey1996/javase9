package chapteres.chap08.task10;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class App {
    public static void main(String[] args) throws IOException {

        List<String> list = new ArrayList<>();
        list.add("efrgrg fefrfrf defefff");
        list.add("efrgrg fefrfrf defefff");
        list.add("efrgrg fefrfrf defefff");
        list.add("efrgrg fefrfrf defefff");
        list.add("efrgrg fefrfrf defefff");

        Optional<Integer> lenght = list.stream().map(w -> w.length()).reduce((x,y) -> x + y);
        long count = list.stream().count();
        System.out.println(lenght.get()/count);

        IntSummaryStatistics summary  = list.stream().collect(Collectors.summarizingInt(String::length));
        System.out.println(summary.getAverage());

    }
}
