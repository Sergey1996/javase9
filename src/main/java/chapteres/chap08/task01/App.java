package chapteres.chap08.task01;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

public class App {
    public static void main(String[] args) throws IOException {

        String contents = new String(Files.readAllBytes(Paths.get("reader01")));
        List<String> words = Arrays.asList(contents.split("\\PL+"));
        int count = 0;
        for(String w : words){
            if(w.length() > 5) count++;
        }
        System.out.println(count);

        long count1 = words.stream().filter(w -> w.length()>5).limit(2).count();
        System.out.println(count1);
    }
}
