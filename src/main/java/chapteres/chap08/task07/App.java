package chapteres.chap08.task07;

import static chapteres.chap08.task05.App.codePoints;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.stream.Stream;

public class App {
    public static void main(String[] args) throws IOException {
        String contents = new String(Files.readAllBytes(Paths.get("reader")));
        Stream<String> words = Stream.of(contents.split("\\s+")).filter(w -> codePoints(w).allMatch(x -> Character.isAlphabetic(x.charAt(0)))).limit(100);
        System.out.println(Arrays.toString(words.toArray()));

    }
}
