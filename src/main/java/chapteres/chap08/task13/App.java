package chapteres.chap08.task13;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class App {
    public static <T> Stream<T> zip(Stream<T> first, Stream<T> second){
        List<T> list1 = first.collect(Collectors.toList());
        List<T> list2 = second.collect(Collectors.toList());
        List<T> result = new ArrayList<>();
        Iterator<T> iterator1 = list1.iterator();
        Iterator<T> iterator2 = list2.iterator();
        while (iterator1.hasNext() || iterator2.hasNext()){
            if(iterator1.hasNext()) result.add(iterator1.next());
            if(iterator2.hasNext()) result.add(iterator2.next());
        }
        return result.stream();
    }

    public static void main(String[] args) {
        System.out.println(Arrays.toString(zip(Stream.of(1,3,5,7,9),Stream.of(2,4,6)).toArray()));
    }
}
