package chapteres.chap08.task14;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class App {

    private static<T> ArrayList<T> merge(List<T> list1,List<T> list2){
        ArrayList<T> result = new ArrayList<>();
        result.addAll(list1);
        result.addAll(list2);
        return result;
    }

    public static<T> List<T> method1(Stream<List<T>> stream, List<T> list){

        return stream.reduce(list,(list1,list2) -> merge(list1,list2));
    }

    public static<T> List<T> method2(Stream<List<T>> stream, List<T> list){

        return merge(list,(stream.reduce((list1,list2) -> merge(list1,list2)).get()));
    }

    public static<T> List<T> method3(Stream<List<T>> stream, List<T> list){

        return stream.reduce(list,(list1,list2) -> merge(list1,list2),(p,q) -> merge(p,q));
    }

    public static void main(String[] args) {
        System.out.println(method1(Stream.of(Arrays.asList("1","2","3"),Arrays.asList("4","5","6"),Arrays.asList("7")),Arrays.asList("-1","0")));
        System.out.println(method2(Stream.of(Arrays.asList("1","2","3"),Arrays.asList("4","5","6"),Arrays.asList("7")),Arrays.asList("-1","0")));
        System.out.println(method3(Stream.of(Arrays.asList("1","2","3"),Arrays.asList("4","5","6"),Arrays.asList("7")),Arrays.asList("-1","0")));

    }
}
