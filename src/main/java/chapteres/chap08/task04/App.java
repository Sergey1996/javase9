package chapteres.chap08.task04;

import java.util.Arrays;
import java.util.stream.LongStream;


public class App {

    private static LongStream method(long a, long c, long m){
        long x0 = 1;
       return LongStream.iterate(x0, x -> (a*x + c) % m);
    }

    public static void main(String[] args) {
        long a = 25214903;
        long c = 11;
        long m = (long) Math.pow(2,48);
        System.out.println(Arrays.toString(method(a,c,m).limit(10).toArray()));
    }
}
