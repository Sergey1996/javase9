package chapteres.chap08.task02;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

public class App {
    public static void main(String[] args) throws IOException {

        String contents = new String(Files.readAllBytes(Paths.get("reader01")));
        List<String> words = Arrays.asList(contents.split("\\PL+"));
        long start = System.currentTimeMillis();
        long count = words.stream().filter(w -> w.length()>5).count();
        long finish = System.currentTimeMillis();
        System.out.println(finish - start);
        start = System.currentTimeMillis();
        count = words.parallelStream().filter(w -> w.length()>5).count();
        finish = System.currentTimeMillis();
        System.out.println(finish - start);
    }

}
