package chapteres.chap08.task11;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class App {
    public static void main(String[] args) {
        Stream<String> stream = Stream.of("ff","frfrfr","deee","efrfrf");
        Optional<Map.Entry<Integer, List<String>>> max = stream.collect(Collectors.groupingBy(String::length))
                .entrySet().stream().max(Map.Entry.comparingByKey());
        System.out.println(max.get().getValue());

    }
}
