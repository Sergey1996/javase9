package chapteres.chap08.task17;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.stream.Stream;

public class App {
    public static void main(String[] args) throws IOException {
        String contents = new String(Files.readAllBytes(Paths.get("reader")));
        System.out.println(Arrays.toString(Stream.of(contents.split("\\s+")).parallel()
                .sorted((s1,s2)-> s1.length() > s2.length() ? -1 : 1).limit(4).toArray()));
    }
}
