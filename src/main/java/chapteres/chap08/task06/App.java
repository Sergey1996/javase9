package chapteres.chap08.task06;

import static chapteres.chap08.task05.App.codePoints;

public class App {

    public static boolean checkStr(String str) {
        return codePoints(str).allMatch(x -> Character.isAlphabetic(x.charAt(0)));
    }

    public static boolean checkJavaIdentif(String str) {
        return codePoints(str).allMatch(x -> Character.isAlphabetic(x.charAt(0)) && x.matches("^[a-zA-Z0-9]+$"));
    }

    public static void main(String[] args) {

        System.out.println(checkStr("boat"));
        System.out.println(checkStr("boat2itr"));

        System.out.println(checkJavaIdentif("thisJAVAidentifier"));
        System.out.println(checkJavaIdentif("этоНЕДЖАВАидентификатор"));
        System.out.println(checkJavaIdentif("this not JAVA identifier"));

    }
}
