package chapteres.chap08.task16;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.stream.Stream;

public class App {

    public static void main(String[] args) {
        Stream<BigInteger> myStream =  Stream.iterate(BigInteger.valueOf((long)(Math.pow(10,100000))),
                x -> x.add(BigInteger.valueOf(1))).limit(50);
        System.out.println(Arrays.toString(myStream.parallel().filter(x -> x.isProbablePrime(1)).limit(500).toArray()));
    }
}
