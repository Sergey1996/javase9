package chapteres.chap08.task09;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.regex.Pattern;


public class App {
    public static void main(String[] args) throws IOException {

        System.out.println(Arrays.toString(Files.lines(Paths.get("words"))
                .filter(s -> Pattern.compile("^[aeyuio]+[a-z]+[aeyuio]+[a-z]+[aeyuio]+[a-z]" +
                        "+[aeyuio]+[a-z]+[aeyuio]").matcher(s).matches()).toArray()));


    }
}
