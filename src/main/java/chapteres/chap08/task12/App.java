package chapteres.chap08.task12;

import java.util.stream.Stream;

public class App {
    public static <T> boolean isFinite(Stream<T> stream){
        return stream.spliterator().estimateSize() != Long.MAX_VALUE;
    }

    public static void main(String[] args) {
        System.out.println(isFinite(Stream.iterate(1, x -> x + 1)));
    }
}
