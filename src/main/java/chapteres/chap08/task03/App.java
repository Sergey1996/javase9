package chapteres.chap08.task03;

import java.util.Arrays;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class App {
    public static void main(String[] args) {
        int [] values = {1, 4, 9, 16};
        System.out.println(Stream.of(values));
        System.out.println(Arrays.toString(Stream.of(values).toArray()));
        System.out.println(IntStream.of(values));
        System.out.println(Arrays.toString(IntStream.of(values).toArray()));

    }
}
