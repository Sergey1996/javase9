package chapteres.chap08.task18;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.stream.Stream;

public class App {

    public static void main(String[] args) throws IOException {
        String contents = new String(Files.readAllBytes(Paths.get("reader")));
        System.out.println(Arrays.toString(Stream.of("1","2","3","3","3","4","5","5").distinct().toArray()));
    }
}
