package chapteres.chap08.task05;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class App {

    public static Stream<String> codePoints(String s){
        List<String> result = new ArrayList<>();
        int i = 0;
        while (i < s.length()) {
            int j = s.offsetByCodePoints(i,1);
            result.add(s.substring(i,j));
            i = j;
        }
        return result.stream();
    }

    public static Stream<String> myCodePoints(String s){
        return IntStream.iterate(0, x -> s.offsetByCodePoints(x,1)).limit(s.length()).mapToObj(x -> s.substring(x,x+1));
    }

    public static void main(String[] args) {
        System.out.println(Arrays.toString(codePoints("boat").toArray()));
        System.out.println(Arrays.toString(myCodePoints("boat").toArray()));
    }
}
