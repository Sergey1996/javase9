package chapteres.chap08.task15;

import java.util.Arrays;
import java.util.stream.Stream;

public class App {
    public static void main(String[] args) {
        Stream<Double> myStream = Stream.iterate(Double.valueOf(0), x -> x + new Double(5)).limit(10);
        System.out.println(myStream.mapToDouble((x)-> x).average().getAsDouble());
        
    }
}
