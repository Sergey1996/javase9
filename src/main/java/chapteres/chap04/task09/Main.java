package chapteres.chap04.task09;

import chapteres.chap04.task01.Point;
import chapteres.chap04.task04.Circle;

import java.lang.reflect.Field;
import java.util.Objects;

public class Main {


    public static String toString(Object object) throws IllegalAccessException {
        String fields = "";
        for(Field f : object.getClass().getDeclaredFields()){
            f.setAccessible(true);
            fields+= "{ " + f.getName() + " : " + f.get(object) + "} ";
        }
        Object objectSup = object.getClass().getSuperclass();
        while (objectSup!=null) {
            for (Field fs : objectSup.getClass().getDeclaredFields()) {
                fs.setAccessible(true);
                fields += "{ " + fs.getName() + " : " + fs.get(object) + "} ";
            }
            objectSup = objectSup.getClass().getSuperclass();
        }
        return fields;
    }

    public static void main(String [] args) throws IllegalAccessException {
        Point point = new Point(3,7);
        Circle cir = new Circle(point, 5);
        System.out.println(toString(point));
        System.out.println(toString(cir));
    }
}
