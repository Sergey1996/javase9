package chapteres.chap04.task13;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.function.DoubleFunction;

public class Main {

    public static void printMethods(Method method, double min, double max, double h) throws InvocationTargetException, IllegalAccessException {
        for(double i = min; i <= max; i += h){
            System.out.println(i + "  :  "+method.invoke(method.getDeclaringClass(),i));
        }
    }

    public static void main(String [] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method sqrtMethod = Math.class.getMethod("sqrt",double.class);
        printMethods(sqrtMethod,200,1000,50);
        System.out.println("------------------ toHexString----------------------");
        Method toHasStringMetod = Double.class.getMethod("toHexString",double.class);
        printMethods(toHasStringMetod, 100,4000,398);

    }
}
