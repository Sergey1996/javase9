package chapteres.chap04.task01;

import java.util.Objects;

public class Point {

    protected double x;
    protected double y;

    public Point(double x, double y){
        this.x = x;
        this.y = y;
    }

    public double getX(){
        return this.x;
    }

    public double getY(){
        return this.y;
    }

    public void setX(double x){
        this.x = x;
    }

    public  void  setY(double y){
        this.y = y;
    }

    public String toString(){
        return this.getClass().getName() + " [ x = " + this.x + " , y = " + this.y + " ]";
    }

    public boolean equals(Object obj){
        System.out.println("equals");
        if(this == obj) return true;
        if (obj == null) return false;
        if (this.getClass() != obj.getClass()) return false;
        Point point = (Point)obj;
        return (point.getX() == this.getX() && this.getY() == point.getY());
    }

    public int hashCode(){
        System.out.println("hashCode" + Objects.hash(this.x,this.y));
        return Objects.hash(this.x,this.y);
    }
}
