package chapteres.chap04.task01;

import java.util.Objects;

public class LabeledPoint extends Point {

    private String label;

    public LabeledPoint(String label, double x, double y) {
        super(x, y);
        this.label=label;

    }

    public String getLabel(){
        return this.label;
    }

    @Override
    public String toString(){
        return super.toString() + label;
    }

    @Override
    public boolean equals(Object object){
        if(! super.equals(object)) return false;
        else {
            if (this == object) return true;
            if (this.getClass() == object.getClass()) return true;
            LabeledPoint labeledPoint = (LabeledPoint) object;
            return Objects.equals(this.label, labeledPoint.label);
        }
    }

    @Override
    public int hashCode(){
        return Objects.hash(this.label);
    }
}
