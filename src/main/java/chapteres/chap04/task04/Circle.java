package chapteres.chap04.task04;

import chapteres.chap04.task01.Point;

public class Circle extends Shape {

    private double redius;

    public Circle(Point center, double redius){
        super(center);
        this.redius = redius;
    }

    @Override
    public Point getCenter() {
        return super.point;
    }

    public Circle clone(){
        return new Circle(super.point,this.redius);
    }
}
