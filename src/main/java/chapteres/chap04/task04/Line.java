package chapteres.chap04.task04;

import chapteres.chap04.task01.Point;

public class Line extends Shape {

    Point from;

    public Line(Point from, Point to) {
        super(to);
        this.from = from;
    }

    @Override
    public Point getCenter() {
        double x = super.point.getX() - this.from.getX();
        double y = super.point.getY() - this.from.getY();
        return new Point(x,y);
    }

    public Line clone(){
        return new Line(this.from, super.point);
    }
}
