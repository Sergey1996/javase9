package chapteres.chap04.task04;

import chapteres.chap04.task01.Point;

public class Rectangle extends Shape {

    double width;
    double height;

    public Rectangle(Point topLeft, double width, double height) {
        super(topLeft);
        this.width = width;
        this.height = height;
    }

    @Override
    public Point getCenter() {
        double x = super.point.getX() + this.width / 2;
        double y = super.point.getY() - this.height/2;
        return new Point(x,y);
    }

    public Rectangle clone(){
        return new Rectangle(super.point,this.width,this.height);
    }
}
