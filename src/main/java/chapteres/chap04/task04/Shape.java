package chapteres.chap04.task04;

import chapteres.chap04.task01.Point;

public abstract class Shape {

    Point point;

    Shape(Point point){
        this.point = point;
    }

    public void moveBy(double dx, double dy){
        point.setX(point.getX() + dx);
        point.setY(point.getY() + dy);
    }

    public abstract Point getCenter();

}
