package chapteres.chap04.task10;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;

public class Main {

    public static void main(String [] args) throws ClassNotFoundException {
       Class cl = Class.forName(int [].class.getName());
           for(Method method : cl.getDeclaredMethods()){
               System.out.println(
                       Modifier.toString(method.getModifiers()) + " " +
                               method.getReturnType().getCanonicalName() + " " + method.getName() + Arrays.toString(method.getParameters()));
           }
    }
}
