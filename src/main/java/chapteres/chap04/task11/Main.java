package chapteres.chap04.task11;

import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Objects;

public class Main  {
    public static void main(String [] args) throws NoSuchFieldException, NoSuchMethodException, InvocationTargetException, IllegalAccessException, FileNotFoundException {
        Field out = System.class.getField("out");
        System.out.println(out.getType());
        Class type = out.getType();
        Method method = type.getMethod("println",String.class);
        PrintStream printStream = new PrintStream(System.out);
        method.invoke(printStream,"Hello Word");
    }
}
