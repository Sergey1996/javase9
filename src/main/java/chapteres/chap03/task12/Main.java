package chapteres.chap03.task12;

import java.io.File;
import java.util.Arrays;

public class Main {

    public static String [] getFileExt(String path, String ext){
        File file = new File(path);
        return file.list((dir, name) -> name.endsWith(ext));
    }

    public static void main(String [] args){
       System.out.println(Arrays.toString(getFileExt("C:/Windows","log")));
    }
}
