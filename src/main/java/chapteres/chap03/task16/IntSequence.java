package chapteres.chap03.task16;

public interface IntSequence {
    boolean hasNext();
    int next();
}