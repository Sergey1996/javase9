package chapteres.chap03.task06;

import java.math.BigInteger;

public interface Sequence<T> {
    boolean hasNext();
    BigInteger next();
}
