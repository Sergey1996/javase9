package chapteres.chap03.task06;

import java.math.BigInteger;

public class SquareSequence implements Sequence<BigInteger> {

    private BigInteger i;

    @Override
    public boolean hasNext() {
        return true;
    }

    @Override
    public BigInteger next() {
        i.add(BigInteger.valueOf(1));
        return i.multiply(i);
    }
}
