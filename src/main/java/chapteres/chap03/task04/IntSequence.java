package chapteres.chap03.task04;

public interface IntSequence {
    boolean hasNext();
    int next();
    public static IntSequence of(int ... values){
        return new IntSequence() {
            int [] mas = values;
            int iter = 0;
            @Override
            public boolean hasNext() {
                return iter < values.length ? true : false;
            }

            @Override
            public int next() {
                if(hasNext()) {
                    int v = mas[iter];
                    iter++;
                    return v;
                } else return 0;
                }
            };
        }
}
class Main {
    public static void main(String[] args) {
        IntSequence sequence = IntSequence.of(2,5,6,8,9);
        while (sequence.hasNext()) {
            System.out.println(sequence.next());
        }
    }
}

