package chapteres.chap03.task15;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Queue;

public class Main {
    public static void main(String [] args){
       Employee emp1 = new Employee("Ivanov",15300);
       Employee emp10 = new Employee("Ivanov",15301);
       Employee emp2 = new Employee("Petrov", 13800);
       Employee emp3 = new Employee("Sidrov",18900);
       Employee emp4 = new Employee("abc",15301);
       Employee emp5 = new Employee("Ivanovzxy", 15299);
       Employee [] massEmployee = {emp1,emp2,emp3,emp4,emp5,emp10};
       Arrays.sort(massEmployee);
       for(Employee employee : massEmployee){
           System.out.println(employee.getName()+" : "+employee.getSalary());
       }
    }
}
