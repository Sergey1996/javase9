package chapteres.chap03.task15;

public interface Measurable {
    double getMeasure();
}
