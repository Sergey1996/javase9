package chapteres.chap03.task14;

public class Main {

    public static Runnable getRun(Runnable [] runnables){
        Runnable oneRun = () -> {
            for(Runnable run : runnables)
                run.run();
        };
        return oneRun;
    }

    public static void main(String [] args){

    }
}
