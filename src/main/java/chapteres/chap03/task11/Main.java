package chapteres.chap03.task11;

import java.io.File;
import java.io.FileFilter;
import java.lang.reflect.Array;

public class Main {

    public static File[] getListDir(String path){
        File file = new File(path);
        return file.listFiles(File::isDirectory);
    }

    public static File[] getListDir1(String path){
        File file = new File(path);
        return file.listFiles(file1 -> file1.isDirectory());
    }


    public static void main(String [] str) {
        File [] files = getListDir1("C:/Windows");
        for(int i=0; i<files.length; i++) {
            System.out.println(files[i].getName());
        }
    }
}
