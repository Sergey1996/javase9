package chapteres.chap03.task09;

public class Main {



    public static void main(String [] args){
        Greeter greeter1 = new Greeter(5,"cat");
        Greeter greeter2 = new Greeter(5, "dog");
        /*Thread thread1 = new Thread(greeter1);
        Thread thread2 = new Thread(greeter2);
        thread1.start();
        thread2.start();*/
        Greeter.runTogether(greeter1,greeter2);
        Greeter.runInOrder(greeter1,greeter2);
    }
}
