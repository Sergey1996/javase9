package chapteres.chap03.task09;

public class Greeter implements Runnable {

    int n;
    String target;

    public Greeter(int n, String target){
        this.n = n;
        this.target = target;
    }

    @Override
    public void run() {
        for(int n = this.n; n > 0; n--){
            System.out.println("Hello, " + this.target);
        }
    }

    //task10

    public static void runTogether(Runnable ... tasks){
        for (Runnable runnable : tasks){
            Thread thread = new Thread(runnable);
            thread.start();
        }
    }

    public static void runInOrder(Runnable ... tasks){
        for(Runnable runnable : tasks){
            runnable.run();
        }

    }

    //.
}
