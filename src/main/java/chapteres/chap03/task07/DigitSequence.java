package chapteres.chap03.task07;

import java.util.Iterator;

public class DigitSequence implements Iterator<Iterator> {

    int number;

    public DigitSequence(int number){
        this.number=number;
    }

    @Override
    public boolean hasNext() {
        return number!=0;
    }

    @Override
    public Iterator next() {
        int val = number % 10;
        this.number /= 10;
        return new DigitSequence(number);
    }
}
