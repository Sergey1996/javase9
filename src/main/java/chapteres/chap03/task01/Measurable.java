package chapteres.chap03.task01;

public interface Measurable {
    double getMeasure();
}
