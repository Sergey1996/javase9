package chapteres.chap03.task01;

import java.lang.reflect.Array;
import java.util.Arrays;

public class Employee implements Measurable,Comparable<Employee> {

    private  String name;
    private double salary;

    public String getName() {
        return name;
    }

    public double getSalary() {
        return salary;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public Employee(String name, double salary){
        this.name=name;
        this.salary=salary;
    }

    public Employee(){}

    @Override
    public double getMeasure() {
        return this.salary;
    }

    double average(Measurable [] objects){
        double val=0;
        for(Measurable measurable:objects){
            val+=measurable.getMeasure();
        }
        return objects.length > 0 ? val/objects.length : 0;
    }

    Measurable largest(Measurable [] object){
        Arrays.sort(object);
        return object[object.length-1];
    }

    @Override
    public int compareTo(Employee o) {
        return Double.compare(getSalary(),o.getSalary());
    }
}
