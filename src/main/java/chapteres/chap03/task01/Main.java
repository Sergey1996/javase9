package chapteres.chap03.task01;

import java.util.Scanner;

public class Main {
    public static void main(String [] args){
        Measurable [] measurables = new Employee [5];
        Employee employee = new Employee();
        for(int i =0; i<measurables.length; i++){
            employee = new Employee("Name"+i, 20000 + i*2000);
            measurables[i] = employee;
        }
        System.out.println(employee.average(measurables));
        Employee largestEmpl = (Employee) employee.largest(measurables);
        System.out.println(largestEmpl.getName());

    }
}
