package chapteres.chap06.task13;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Lists {

    public static <T> void minmax(List<T> elements, Comparator<? super T> comp, List<? super T> result){
        T min = elements.get(0);
        for(T val : elements){
            if(comp.compare(val,min) == 1){
                min = val;
            }
        }
        T max = elements.get(0);
        for(T val : elements){
            if(comp.compare(val,max) == -1){
                max = val;
            }
        }
        result.add(min);
        result.add(max);
    }

    public static <T> void maxmin(List<T> elements, Comparator<? super T> comp, List<? super T> result){
        minmax(elements,comp,result);
        swapHelper(result,0,1);
    }

    private static <T> void swapHelper(List<T> elements, int i, int j){
        T temp = elements.get(i);
        elements.set(i,elements.get(j));
        elements.set(j,temp);
    }
}
