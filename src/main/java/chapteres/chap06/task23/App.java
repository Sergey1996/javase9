package chapteres.chap06.task23;

import java.util.concurrent.Callable;

import javax.naming.OperationNotSupportedException;

public class App {
    public static class Exceptions {
        @SuppressWarnings("unchecked")
        private static <T extends Throwable> void throwAs(Throwable e) throws T {
            throw  (T) e;
        }
        public static <V> V doWork(Callable<V> c){
            try{
                return c.call();
            } catch (Throwable ex) {
                Exceptions.throwAs(ex);
                return null;
            }
        }
    }

    public static void main(String[] args) {
        Exceptions.throwAs(new OperationNotSupportedException());
    }
}
