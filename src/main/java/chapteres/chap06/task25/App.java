package chapteres.chap06.task25;

import java.lang.reflect.*;
import java.util.Arrays;

public class App {
    public static String genericDeclaration(Method m){
        String description = "";
        TypeVariable<Method> [] vars = m.getTypeParameters();
        if(vars.length > 0) {
            Type[] bounds = vars[0].getBounds();
            if (bounds.length > 0) {
                description = m.getName() + " : ";
                description += bounds[0].getTypeName() + " ";
            }
            if (bounds[0] instanceof ParameterizedType) {
                //Comparable<? extends T>
                ParameterizedType p = (ParameterizedType) bounds[0];
                Type[] typeArguments = p.getActualTypeArguments();
                if (typeArguments.length > 0) {
                    description += typeArguments[0].getTypeName()+ " ";
                }
                if (typeArguments[0] instanceof WildcardType) {
                    // ? super T
                    WildcardType t = (WildcardType) typeArguments[0];
                    Type[] upper = t.getUpperBounds();
                    if (upper.length > 0) {
                        description += upper[0].getTypeName()+ " ";
                    }
                    // ? extends ...&...
                    Type[] lower = t.getLowerBounds();
                    // ? super  ...&...
                    if (lower.length > 0) {
                        description += lower[0].getTypeName()+ " ";
                    }
                }
            }
        }
        return description;
    }

    public static void main(String[] args){
        Method [] massMet = Arrays.class.getDeclaredMethods();
        String res;
        for(Method m: massMet){
            res = genericDeclaration(m);
            if(res.equals("") == false)
            System.out.println(res);
        }
    }
}
