package chapteres.chap06.task15;

import chapteres.chap04.task01.Point;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.function.Function;

public class Main {

    public static <T,R> ArrayList<R> map(ArrayList<T> list, Function<T, R> function){
        ArrayList<R> result = new ArrayList<>();
        for(T t : list ){
            result.add(function.apply(t));
        }
        return result;
    }

    static class MyFunc<T,R> implements Function<T,R>{

        MyFunc(){}

        @Override
        public R apply(T t) {
            return (R)t.getClass();
        }
    }

    public static void main(String[] args) {
          Integer num = new Integer(56);
          Point point = new Point(7,4);
          ArrayList<Point> list = new ArrayList<>();
          list.add(point);
          MyFunc func = new MyFunc<>();
          System.out.println(map(list,func));
    }
}
