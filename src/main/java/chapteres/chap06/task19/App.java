package chapteres.chap06.task19;

import java.util.ArrayList;

public class App {
    public static <T> ArrayList<T> repeat(int n, T obj){
        ArrayList<T> result = new ArrayList<>();
        for(int i = 0; i < n; i++){
            result.add(obj);
        }
        T [] mass = (T[]) result.toArray();
        return result;
    }
}
