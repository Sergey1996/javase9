package chapteres.chap06.task22;

import java.io.IOException;
import java.util.concurrent.Callable;

import javax.naming.OperationNotSupportedException;

public class App {
    public static <V,T extends Exception> V doWork(Callable<V> c, T ex) throws T{
        try{
            return c.call();
        }catch (Throwable realEx){
            ex.initCause(realEx);
            throw ex;
        }
    }

    public static void main(String[] args) throws Exception {

        doWork(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                throw new IOException();
            }
        }, new OperationNotSupportedException());
    }
}
