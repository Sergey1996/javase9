package chapteres.chap06.task10;

public class Arrays{

    public static <T extends Comparable> T min( T [] mass){
        T min = mass[0];
        for(T val : mass){
            if(min.compareTo(val) == -1){
                min = val;
            }
        }
        return min;
    }

    public static <T extends Comparable> T max( T [] mass){
        T max = mass[0];
        for(T val : mass){
            if(max.compareTo(val) == 1){
                max = val;
            }
        }
        return max;
    }
}
