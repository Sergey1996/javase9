package chapteres.chap06.task02;

import java.util.Arrays;

public class Stack<E> {

    private E [] mass;
    private int index = -1;

    public Stack(){
        this.mass = (E[]) new Object[10];
    }

    public void push(E value){
        index++;
        if(mass.length > index){
            mass[index] = value;
        }
        else{
            mass = Arrays.copyOf(mass,mass.length*2);
            mass[index] = value;
        }
    }

    public E pop(){
        E value = mass[index];
        mass[index] = null;
        index --;
        return value;
    }

    public boolean isEmpty(){
        return mass[index] == null ? false : true;
    }
}
