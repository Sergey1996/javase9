package chapteres.chap06.task04;

import java.util.ArrayList;

public class Table<K,V> extends ArrayList<Object> {

    public class Entry{

        private K key;
        private V value;

        public Entry(K key, V value){
            this.key=key;
            this.value = value;
        }

        public K getKey() {
            return key;
        }

        public V getValue() {
            return value;
        }
    }




    public V getValue(K key){
        V value = null;
        for(int i = 0; i < super.size(); i++)
            if(((Entry)super.get(i)).getKey().equals(key))
                value = ((Entry)super.get(i)).getValue();
        return value;
    }

    public void addEntry(K key, V value){
        boolean flag = true;
        Entry entry = new Entry(key,value);
        for(int i = 0; i < super.size(); i++)
            if(((Entry)super.get(i)).getKey().equals(entry.getKey())) {
                flag = false;
                super.set(i,  entry);
            }
        if(flag) {
            super.add(entry);
        }
    }

    public void deleteEntry(K key){
        for(int i = 0; i < super.size(); i++)
            if(((Entry)super.get(i)).getKey().equals(key))
                super.remove(i);
    }
}
