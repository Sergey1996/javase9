package chapteres.chap06.task04;

public class Main {

    public static void main(String [] args){

        Table<String,String> entryes = new Table<>();
        entryes.addEntry("e1","e1-value");
        entryes.addEntry("e2","e2-value");

        System.out.println(entryes.getValue("e1"));
        System.out.println(entryes.getValue("e2"));
        System.out.println(entryes.getValue("e2"));

        entryes.addEntry("e1","e3-value");
        System.out.println(entryes.getValue("e1"));

    }
}
