package chapteres.chap06.task18;

import java.util.function.IntFunction;

public class App {
    public static <T> T[] repeat(int n, T obj, IntFunction<T[]> constr){
        T[] result = constr.apply(n);
        for(int i = 0; i < n; i++){
            result[i] = obj;
        }
        return result;
    }

    public static void main(String[] args) {
        //repeat(10,42, int[]::new);
        repeat(10,Integer.valueOf(42),Integer[]::new);
    }
}
