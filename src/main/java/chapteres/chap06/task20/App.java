package chapteres.chap06.task20;

import java.util.Arrays;

public class App {
    @SafeVarargs
    public static final <T> T[] repeat(int n, T ... obj){
        @SuppressWarnings("unchecked")
        T[] result = (T[]) java.lang.reflect.Array.newInstance(obj.getClass().getComponentType(),obj.length * n);
        int k = 0;
        for(int j = 0; j < n; j++) {
            for(int i = 0; i < obj.length; i++) {
                result[k] = obj[i];
                k++;
            }
        }
             return result;
    }

    public static void main(String[] args) {
        System.out.println(Arrays.toString(repeat(5,"obj1","obj2","obj3")));
    }
}
