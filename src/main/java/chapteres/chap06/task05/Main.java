package chapteres.chap06.task05;

import java.util.Arrays;

public class Main {

    public static <T> T[] swap(int i, int j, T ... values){
        T temp = values[i];
        values[i] = values[j];
        values[j] = temp;
        return values;
    }

    public static void main(String [] args){

        //Double [] result = (Double[]) swap(0, 1, 1.5, 2, 3);
        //Double [] result = swap(0, 1, 1.5, 2, 3);
        Double [] mass = new Double[3];
        mass[0] = new Double(1.5);
        mass[1] = new Double(2);
        mass[2] = new Double(3);
        Double [] result = (Double[]) swap(0, 1, mass);
        System.out.println(Arrays.toString(result));
    }

}
