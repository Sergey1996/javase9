package chapteres.chap06.task03;

public class Main {

    public static void main(String [] args){
        Entry<String,String> e1 = new Entry<>("e1","e1-value");
        Entry<String,String> e2 = new Entry<>("e2","e2-value");

        Table<String,String> entryes = new Table<>();
        entryes.addEntry(e1);
        entryes.addEntry(e2);

        System.out.println(entryes.getValue("e1"));
        System.out.println(entryes.getValue("e2"));
        System.out.println(entryes.getValue("e2"));

        Entry<String,String> e3 = new Entry<>("e1","e3-value");
        entryes.addEntry(e3);
        System.out.println(entryes.getValue("e1"));
    }
}
