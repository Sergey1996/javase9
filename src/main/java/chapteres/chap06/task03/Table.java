package chapteres.chap06.task03;

import java.util.ArrayList;

public class Table<K,V> extends ArrayList<Entry<K,V>> {

    public V getValue(K key){
        V value = null;
        for(int i = 0; i < super.size(); i++)
            if(super.get(i).getKey().equals(key))
                value = super.get(i).getValue();
        return value;
    }

    public void addEntry(Entry<K,V> entry){
        boolean flag = true;
        for(int i = 0; i < super.size(); i++)
            if(super.get(i).getKey().equals(entry.getKey())) {
                flag = false;
                super.set(i,entry);
            }
        if(flag) {
            super.add(entry);
        }
    }

    public void deleteEntry(K key){
        for(int i = 0; i < super.size(); i++)
            if(super.get(i).getKey().equals(key))
                super.remove(i);
    }
}
