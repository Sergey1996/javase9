package chapteres.chap06.task21;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class App {

     @SafeVarargs
    static void m(List<String>... stringLists) {
        Object[] array = stringLists;
        List<Integer> tmpList = Arrays.asList(42);
        array[0] = tmpList; // Semantically invalid, but compiles without warnings
        String s = stringLists[0].get(0); // Oh no, ClassCastException at runtime!
    }

    @SafeVarargs
    public static <T extends List<String>> T[] construct(Object ... val){
        T t = null;
        return (T[]) java.lang.reflect.Array.newInstance(val.getClass().getComponentType(),val.length);
    }

    public static void main(String[] args) {
        //List<String>[] result = construct(10);

        m(Arrays.asList("Java", "Scala", "Groovy"));
    }
}
