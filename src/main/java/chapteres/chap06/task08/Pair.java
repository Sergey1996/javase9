package chapteres.chap06.task08;

import java.util.Objects;

public class Pair<E extends Comparable> {

    private E e1;
    private E e2;

    //public Pair() {}

    public Pair(E e1, E e2) {
        this.e1 = e1;
        this.e2 = e2;
    }

    public E getE1() {
        return e1;
    }

    public E getE2() {
        return e2;
    }

    public E max(){
        if(e1.compareTo(e2) == 1) return e1;
        else return e2;
    }
    public E min(){
        if(e1.compareTo(e2) == 1) return e2;
        else return e1;
    }

}
