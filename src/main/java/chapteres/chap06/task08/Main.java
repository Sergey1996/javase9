package chapteres.chap06.task08;

import chapteres.chap06.task08.Pair;

public class Main {

    public static void main(String[] args) {
        Pair<Integer> pair = new Pair<>(5,8);
        System.out.println(pair.max() + "  " + pair.min());
    }

}
