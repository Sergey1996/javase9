package chapteres.chap06.task16;

import java.io.Serializable;
import java.util.*;

public class Main {

    //Когда определяется обобщенный тип он компилируется в базовый тип например <T> -> Object,
    // а <T extends Comparable> -> Comparable

    public static <T extends Comparable<? super T>> void sort(List<T> list) {
                       //Comparable                              //Object
    }

    public static <T extends Object & Comparable<? super T>> T max(Collection<? extends T> coll) {
                           //Comparable                                         //Object

        return null;
    }

    public static void main(String[] args) {
        sort(new ArrayList<String>());
        max(new ArrayList<String>());
    }
}
