package chapteres.chap06.task01;

import java.util.ArrayList;

public class Stack<E> extends ArrayList<E> {

    private int cur = -1;


    public void push(E value){
        cur++;
        super.add(value);
    }
    public E pop(){
            int ind = cur;
            E value = super.get(ind);
            cur--;
            return value;
    }

    public boolean isEmpty(){
        return super.isEmpty();
    }
}
