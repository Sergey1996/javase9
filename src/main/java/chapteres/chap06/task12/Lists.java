package chapteres.chap06.task12;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class Lists {

    public static <T> void minmax(List<T> elements, Comparator<? super T> comp, List<? super T> result){
        T min = elements.get(0);
        for(T val : elements){
            if(comp.compare(val,min) == 1){
                min = val;
            }
        }
        T max = elements.get(0);
        for(T val : elements){
            if(comp.compare(val,max) == -1){
                max = val;
            }
        }
        result.add(min);
        result.add(max);
    }
}
