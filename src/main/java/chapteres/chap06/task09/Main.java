package chapteres.chap06.task09;

import java.util.ArrayList;
import java.util.Arrays;

public class Main {
   public static <T extends Comparable> Pair<T> firstLast(ArrayList<? extends Comparable> a){
       return new Pair(a.get(0),a.get(a.size()-1));
   }
}
