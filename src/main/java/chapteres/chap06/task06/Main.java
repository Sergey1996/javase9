package chapteres.chap06.task06;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import chapteres.chap04.task01.LabeledPoint;
import chapteres.chap04.task01.Point;
import chapteres.chap04.task04.Circle;
import chapteres.chap04.task04.Shape;

public class Main {

    public static <E> ArrayList<E> join1(ArrayList<E> mass1, ArrayList<? extends E> mass2){
        for(int i = 0; i < mass2.size(); i++){
          mass1.add(mass2.get(i));
        }
      return mass1;
    }

    public static <E> ArrayList<? super E> join2(ArrayList<E> mass1, ArrayList<? super E> mass2){
        for(int i = 0; i < 2; i++){
            mass2.add(mass1.get(i));
        }
        return mass2;
    }

    public static void main(String[] args) {
        ArrayList<LabeledPoint> list1 = new ArrayList<>();
        ArrayList<Point> list2 = new ArrayList<>();
        ArrayList<Point> list3 = new ArrayList<>();
        list1.add(new LabeledPoint("label",5,7));
        list2.add(new Point(3,6));
        list3.add(new Point(7,8));
        System.out.println(join1(list2,list1));
        System.out.println(join2(list2,list3));
    }
}
