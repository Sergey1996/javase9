package chapteres.chap05.task15;

import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

public class HTMLFormater extends Formatter {


    @Override
    public String format(LogRecord record) {
        Date date = new Date();
        return ("  <tr>\n" +
                "  <td>" + date.toString() + "</td>\n" +
                "  <td>" + record.getMillis() + "</td>\n" +
                "  <td>" + record.getSequenceNumber() + "</td>\n" +
                "  <td>" + record.getLoggerName() + "</td>\n" +
                "  <td>" + record.getLevel() + "</td>\n" +
                "  <td>" + record.getSourceClassName() + "</td>\n" +
                "  <td>" + record.getSourceMethodName() + "</td>\n" +
                "  <td>" + record.getThreadID() + "</td>\n" +
                "  <td>" + record.getMessage() + "</td>\n"  +
                "  </tr>\n");
    }
    public String getHead(Handler h) {

        return ("<!DOCTYPE html>\n" +
                "<html>\n" +
                "   <head>\n" +
                "      <meta charset=\"utf-8\" />\n" +
                "      <title>HTML Document</title>\n" +
                "   </head>\n" +
                "   <body>\n" +
                "       <table border=\"1\">\n" +
                "            <tr>\n" +
                "            <th>date</th>\n" +
                "            <th>millis</th>\n" +
                "            <th>sequence</th>\n" +
                "            <th>logger</th>\n" +
                "            <th>level</th>\n" +
                "            <th>class</th>\n" +
                "            <th>method</th>\n" +
                "            <th>thread</th>\n" +
                "            <th>massage</th>\n" +
                "             </tr>\n");
    }

    public String getTail(Handler h) {
        return ("</table>\n" +
                "</body>\n" +
                "</html>");
    }
}
