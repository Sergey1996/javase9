package chapteres.chap05.task05;

import java.io.EOFException;
import java.io.IOException;

public class Application {
    public static void main(String[] args) {

        try {
            System.out.println("first try");
            throw new IOException();
        } catch (IOException io) {
            System.out.println("I am catch ioexceptions: ");
            try {
                System.out.println("I make EOFex");
                throw new EOFException();
            } catch (EOFException e) {
                io.addSuppressed(e);
                for (Throwable t : io.getSuppressed()) {
                    System.out.println(t);
                }
            }
            io.printStackTrace();
        }

    }


}
