package chapteres.chap05.task05;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Paths;
import java.util.Scanner;

public class Main {
    public static void main(String [] args){
        Scanner in = null;
        PrintWriter out = null;
        try{
            in = new Scanner(Paths.get("read"));
            out = new PrintWriter("output.txt");
            while (in.hasNext()){
                out.println(in.next().toLowerCase());
            }
            //out.close();
        } catch (IOException ex1) {
            System.out.println(ex1);
        }
        finally {
            in.close();
            out.close();
        }

    }
}
