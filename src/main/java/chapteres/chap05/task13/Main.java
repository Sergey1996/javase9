package chapteres.chap05.task13;

import java.lang.reflect.Array;
import java.time.Clock;
import java.util.Arrays;
import java.util.Random;

public class Main {

    public static int min(int [] values){
        Arrays.sort(values);
        int min = values[0];

        for(int i = 0; i < values.length; i++){
            assert min <= values[i];
        }

        return min;
    }
    public static void main(String [] args){
        //System.out.println(min(new int[]{4, 6, 1, 3, -5, 89, 4}));
        int [] bigMass = new int [100000];
        Random random = new Random();
        for(int i = 0; i < bigMass.length; i++){
            bigMass[i] = random.nextInt(100)+i;
        }
        long start = System.currentTimeMillis();
        System.out.println(min(bigMass));
        long finish = System.currentTimeMillis();
        System.out.println(finish-start);

    }
}
