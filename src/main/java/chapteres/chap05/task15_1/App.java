package chapteres.chap05.task15_1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class App {
    public static void main(String[] args) {
        Logger logger = LoggerFactory.getLogger("MySLFLog");
        logger.info("hello");
    }
}
