package chapteres.chap05.task01;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    /*public static ArrayList<Double> readValues(String filename) throws IOException {
        ArrayList<Double> values = new ArrayList<>();
        try {
            File file = new File(filename);
            Scanner scanner = new Scanner(file);
            while (scanner.hasNextDouble()) {
                values.add(scanner.nextDouble());
            }
        }
        catch (IOException ex){
            System.err.println("Нет такого файла: " + filename);
        }
        return values;
    }*/

    public static ArrayList<Double> readValues(String filename) throws IOException,NumberFormatException{
        ArrayList<Double> values = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(filename))){
            double num = 0;
            while (reader.ready()) {
                try{
                    String str = reader.readLine();
                    num = Double.parseDouble(str);
                }catch (NumberFormatException ex){
                    throw new NumberFormatException("Строка не число");
                }
                values.add(num);
            }
        }catch (IOException ex){
            throw new IOException("Файл не найден: "+filename);
        }
        return values;
    }

    public static void main(String [] args) throws IOException {
          System.out.println(readValues("read"));
    }
}
