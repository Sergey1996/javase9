package chapteres.chap05.task02;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {


    public static double sumOfValues(String filename) throws IOException {
        double sum = 0;
            ArrayList<Double> values = chapteres.chap05.task01.Main.readValues(filename);
            for (double val : values) {
                sum += val;
            }
        return sum;
    }

    public static void main(String [] args) throws IOException {
        System.out.println(sumOfValues("read"));
    }
}
