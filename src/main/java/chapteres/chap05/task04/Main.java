package chapteres.chap05.task04;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static ArrayList<Double> readValues(String filename) throws IOException {
        ArrayList<Double> values = new ArrayList<>();
        try {
            File file = new File(filename);
            Scanner scanner = new Scanner(file);
            while (scanner.hasNextDouble()) {
                values.add(scanner.nextDouble());
            }
        }
        catch (IOException ex){
            System.out.println(-1);
        }
        return values;
    }

    public static double sumOfValues(String filename) throws IOException {
        double sum = 0;
        try {
            ArrayList<Double> values = new ArrayList<>();
            values = readValues(filename);
            for (double val : values) {
                sum += val;
            }
        }
        catch (IOException ex){
            System.out.println(-1);
        }
        return sum;
    }

    public static void main(String [] args) throws IOException {
        System.out.println(sumOfValues("ready"));
    }
}
