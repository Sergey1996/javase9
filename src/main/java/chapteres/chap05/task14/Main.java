package chapteres.chap05.task14;

import java.io.IOException;
import java.util.logging.*;

public class Main {
    public static class FilterLog implements Filter{

        public FilterLog(){}

        @Override
        public boolean isLoggable(LogRecord record) {
            if(record.getMessage().compareTo("C++") == 0) {
                return false;
            }else return true;
        }
    }

    public static void main(String [] args) throws IOException {
        Logger logger = Logger.getLogger("chap05.task14");
        Handler handler = new FileHandler("log1");
        logger.setFilter(new FilterLog());
        logger.addHandler(handler);
        logger.info("Anton");
        logger.info("java");
        logger.info("C++");
        logger.info("Cat");
    }
}
