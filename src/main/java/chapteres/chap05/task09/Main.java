package chapteres.chap05.task09;

import java.util.concurrent.locks.ReentrantLock;

public class Main {

    public static AutoCloseable metod(){
        ReentrantLock reentrantLock = new ReentrantLock();
        return new AutoCloseable() {
            @Override
            public void close(){
                reentrantLock.unlock();
            }
        };
    }

}
