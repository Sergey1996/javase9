package chapteres.chap02.task06;


public final class Point {

    private double x;
    private double y;

    public Point(double x, double y){
        this.x=x;
        this.y=y;
    }
    public Point(){
        this.x=0.0;
        this.y=0.0;
    }
    public double getX(){
        return this.x;
    }
    public double getY(){
        return this.y;
    }


    public Point translate(double ox, double oy){
        this.x = this.x + ox;
        this.y = this.y + oy;
        return this;
    }


    public Point scale(double scale){
        this.x = this.x*scale;
        this.y = this.y*scale;
        return this;
    }

    public static void print(){
        Point p = new Point(3,4).translate(1,3).scale(0.5);
        System.out.println(p.getX()+";"+p.getY());
    }
}
