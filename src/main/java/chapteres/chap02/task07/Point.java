package chapteres.chap02.task07;


public final class Point {

    private final double x;
    private final double y;

    public Point(double x, double y){
        this.x=x;
        this.y=y;
    }
    public Point(){
        this.x=0.0;
        this.y=0.0;
    }
    public double getX(){
        return this.x;
    }
    public double getY(){
        return this.y;
    }


    public Point translate(double ox, double oy){
        return new Point(this.x + ox,this.y + oy);
    }


    public Point scale(double scale){
        return new Point(this.x*scale,this.y*scale);
    }

    public static void print(){
        Point p = new Point(3,4).translate(1,3).scale(0.5);
        System.out.println(p.getX()+";"+p.getY());
    }

}

