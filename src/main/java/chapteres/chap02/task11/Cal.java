package chapteres.chap02.task11;

import java.time.LocalDate;
import static java.lang.System.*;
import static java.time.LocalDate.*;
public class Cal {

    public static void task11(){
        LocalDate date = now().withDayOfMonth(1);
        int month = date.getMonthValue();
        out.println(" Sun Mon Tue Wed Thu Fri Sat");
        int f = 0;
        while (date.getMonthValue() == month) {
            if (date.getDayOfWeek().getValue() == 1 && f == 0) {
                out.print("    ");
                f=1;
            }
            out.printf("%4d", date.getDayOfMonth());
            date = date.plusDays(1);
            if (date.getDayOfWeek().getValue() == 7)
                out.println();
        }

    }
    public static void main(String [] args){
        task11();
    }
}
