package chapteres.chap02.task16;


public class Queue {

    private static class Node{
        String str;
        Node next;

        Node(String str, Node node){
            this.str = str;
            this.next=node;
        }

        Node(String str){
            this.str = str;
            this.next=null;
        }

        public Node getNext(){
             return this.next;
        }

    }

    private Node head;

    public void add(String str){
        Node newnode = new Node(str);
        newnode.next = head;
        head = newnode;
    }


    public void remove(){
        Node node = head;
        Node prev = head;
        while (node.next != null){
            prev = node;
            node = node.next;
        }
        prev.next = null;

    }

    public String getStr(){
        Node node = head;
        while (node.next != null){
            node = node.next;
        }
        return node.str;
    }

}
