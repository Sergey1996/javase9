package chapteres.chap02.task15;

import java.util.ArrayList;

public class Invoice {

    private static class Item {
        String description;
        int quantity;
        double unitPrice;

        public Item(String description, int quantity, double unitPrice){
            this.description = description;
            this.quantity = quantity;
            this.unitPrice = unitPrice;
        }
        double price(){
            return quantity * unitPrice;
        }


    }

    private ArrayList<Item> items = new ArrayList<>();

    public void addItem(String description, int quantity, double unitPrice){
        Item item = new Item(description,quantity,unitPrice);
        this.items.add(item);
    }

    public void printInvoice(){
        for(Item item : this.items)
        System.out.println("Description: " + item.description + ";  quantity: " + item.quantity + ";  unitPrice: " + item.unitPrice);
    }

}
