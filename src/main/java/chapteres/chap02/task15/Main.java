package chapteres.chap02.task15;

public class Main {
    public static void main(String [] args){
        Invoice invoice = new Invoice();
        invoice.addItem("мячи",10,572);
        invoice.addItem("велосипеды",5,19000);
        invoice.addItem("спортивная форма",15,5000);
        invoice.addItem("тренажеры",5,34000);
        invoice.addItem("штанга",3,16000);
        invoice.printInvoice();
    }
}
