package chapteres.chap02.task01;

import java.time.DayOfWeek;
import java.time.LocalDate;

public class Cal {

    public static  void task1(){
        LocalDate date = LocalDate.now().withDayOfMonth(1);
        int month = date.getMonthValue();
        System.out.println(" Sun Mon Tue Wed Thu Fri Sat");
        DayOfWeek weekday = date.getDayOfWeek();
        int f = 0;
        while (date.getMonthValue() == month) {
            if (date.getDayOfWeek().getValue() == 1 && f == 0) {
                System.out.print("    ");
                f=1;
            }
            System.out.printf("%4d", date.getDayOfMonth());
            date = date.plusDays(1);
            if (date.getDayOfWeek().getValue() == 7)
                System.out.println();
        }

    }
    public static void main(String [] args){
        task1();
    }
}

