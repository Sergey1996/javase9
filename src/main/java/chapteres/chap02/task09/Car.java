package chapteres.chap02.task09;

public class Car {

    private double x;
    private double liters;
    private double consumption;

    public Car(double consumption){
        this.consumption = consumption;
        this.x = 0;
        this.liters = 0;
    }

    public void setX(double x){
        this.x=x;
    }
    public void setLiters(double liters){
        this.liters+=liters;
    }
    public double getX(){
        return this.x;
    }
    public double getLiters(){
        this.liters = this.liters - this.x / this.consumption;
        return this.liters;
    }
}
