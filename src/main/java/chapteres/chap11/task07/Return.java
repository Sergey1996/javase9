package chapteres.chap11.task07;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
public @interface Return {
    String value();
}
