package chapteres.chap11.task07;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Set;

@SupportedAnnotationTypes({"chapteres.chap11.task07.Param", "chapteres.chap11.task07.Return"})
@SupportedSourceVersion(SourceVersion.RELEASE_8)
public class DocumentProcessor extends AbstractProcessor {
    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        if (annotations.size() == 0) return true;

        File doc = new File("documentation.html");
        try(FileWriter writer = new FileWriter(doc)) {
            writer.write("<!DOCTYPE html>\n" +
                    "<html>\n" +
                    "   <head>\n" +
                    "      <meta charset=\"utf-8\" />\n" +
                    "      <title>HTML Document</title>\n" +
                    "   </head>\n" +
                    "   <body>\n");
            for(Element element : roundEnv.getElementsAnnotatedWith(Return.class)) {
                    writer.write("<p>" + element.getSimpleName() + " - " + " Return: "
                            + element.getAnnotation(Return.class).value()
                            + ", Param: " + Arrays.toString(element.getAnnotation(Param.class).value()) + "</p>\n");
            }

            writer.write("</body>\n" +
                    "</html>");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }
}
