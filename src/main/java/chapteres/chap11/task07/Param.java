package chapteres.chap11.task07;

import javax.lang.model.element.Element;
import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
public @interface Param {
    String [] value();
}
