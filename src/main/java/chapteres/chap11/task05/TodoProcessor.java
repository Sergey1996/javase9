package chapteres.chap11.task05;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.tools.JavaFileObject;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

@SupportedAnnotationTypes("chapteres.chap11.task05.Todo")
@SupportedSourceVersion(SourceVersion.RELEASE_8)
public class TodoProcessor extends AbstractProcessor {
    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        if(annotations.size() == 0) return true;
        File file = new File("Todo");
        try (FileWriter fileWriter = new FileWriter(file)) {
            for(Element element : roundEnv.getElementsAnnotatedWith(Todo.class)) {
                fileWriter.write(element.getSimpleName().toString() +  " : ");
                fileWriter.write(element.getAnnotation(Todo.class).todo());
                System.out.println(element.getSimpleName().toString() +  " : "
                        + element.getAnnotation(Todo.class).todo());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        /*try {
            JavaFileObject sourceObj = processingEnv.getFiler().createSourceFile("chapteres.chap11.task05.Todo2");
            try(PrintWriter printWriter = new PrintWriter(sourceObj.openWriter())) {
                for(Element element : roundEnv.getElementsAnnotatedWith(Todo.class)) {
                    printWriter.write(element.getSimpleName().toString());
                    printWriter.write(element.getAnnotation(Todo.class).todo());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }*/
        return false;
    }
}
