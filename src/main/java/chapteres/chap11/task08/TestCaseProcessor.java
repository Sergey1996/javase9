package chapteres.chap11.task08;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.tools.JavaFileObject;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Set;

@SupportedAnnotationTypes("chapteres.chap11.task08.TestCases")
@SupportedSourceVersion(SourceVersion.RELEASE_8)
public class TestCaseProcessor extends AbstractProcessor {
    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        if(annotations.size() == 0) return true;
        try {

            JavaFileObject sourceObj = processingEnv.getFiler().createSourceFile("chapteres.chap11.task08.MyMathTest");
                try (PrintWriter printWriter = new PrintWriter(sourceObj.openWriter())) {
                    printWriter.println("package chapteres.chap11.task08;");
                    printWriter.println("public class MyMathTest {");
                    printWriter.println("public void test() {");
                for (Element element : roundEnv.getElementsAnnotatedWith(TestCases.class)) {
                    for (TestCase testCase : element.getAnnotation(TestCases.class).value()) {
                        printWriter.println("assert(MyMath.factorial(" + testCase.params()
                                + ")==" + testCase.expect() + ");");
                    }
                }
                printWriter.println("}");
                printWriter.println("}");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }
}
