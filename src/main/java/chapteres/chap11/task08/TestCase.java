package chapteres.chap11.task08;

import java.lang.annotation.Repeatable;

@Repeatable(TestCases.class)
public @interface TestCase {
    String params();
    String expect();
}
