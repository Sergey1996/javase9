package chapteres.chap11.task10;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.net.URL;
import java.net.URLConnection;
import java.util.stream.Collectors;


public class MyProcessor {

public static void getResource(Object object) throws IOException, IllegalAccessException {
    for(Field field : object.getClass().getDeclaredFields()) {
        Resource resource = field.getAnnotation(Resource.class);
        if(resource != null) {
            URL url = new URL(resource.URL());
            URLConnection urlConnection = url.openConnection();
            String result = "";
            try (InputStream read = urlConnection.getInputStream()) {
                result = new BufferedReader(new InputStreamReader(read)).lines()
                        .collect(Collectors.joining("\n"));
            }
            field.set(object,result);
        }
    }
}
}
