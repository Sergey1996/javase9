package chapteres.chap11.task10;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException, IllegalAccessException {
        MyClass myClass = new MyClass();
        MyProcessor.getResource(myClass);
        System.out.println(myClass.url);
    }

}
