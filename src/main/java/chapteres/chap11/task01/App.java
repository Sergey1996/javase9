package chapteres.chap11.task01;

import chapteres.chap11.task02.Serializable;

import java.lang.annotation.Annotation;

public class App {
    public static void main(String[] args) throws CloneNotSupportedException {
        Myclass myclass = new Myclass();
        Object object = myclass.myClone();
        System.out.println(object.toString());

    }


    @Cloneable
    static class Myclass implements java.lang.Cloneable {

        @Override
        protected Object clone() throws CloneNotSupportedException {
            return super.clone();
        }

        Object myClone() throws CloneNotSupportedException {
            if(this.getClass().getAnnotation(Cloneable.class) != null) {
                return this.clone();
            } else {
                throw new CloneNotSupportedException();
            }
        }
    }
}
