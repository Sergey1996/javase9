package chapteres.chap11.task09;

import java.lang.annotation.Repeatable;

@Repeatable(TestCases.class)
public @interface TestCase {
    String params();
    String expect();
}
