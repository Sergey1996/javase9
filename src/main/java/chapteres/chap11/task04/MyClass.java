package chapteres.chap11.task04;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Type;

public class MyClass implements Serializable {

    public String str;
    @Transient
    public double value;

    public MyClass(String str, double value) {
        this.str = str;
        this.value = value;
    }

    private void writeObject(ObjectOutputStream out) throws IllegalAccessException, IOException {
        Field[] fields = this.getClass().getFields();
        boolean flag = true;
        for (Field field : fields) {
            for (Annotation annotation : field.getAnnotations()) {
                if (annotation.equals(Transient.class)) {
                    flag = false;
                    break;
                }
                if (!flag) break;
            }
            if (flag) {
                Type type = field.getType();
                if (type.equals(Object.class)) {
                    //out.writeBytes();
                }
            }
        }
    }
}
    
    class Main {
        
        public static void main(String[] args) {

        }
    }

