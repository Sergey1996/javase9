package chapteres.chap11.task02;

import java.io.IOException;
import java.io.ObjectOutputStream;

@Serializable
public class A implements java.io.Serializable {
    int i;
    String name;

    public A(int i, String name) {
        this.i = i;
        this.name = name;
    }

    public void writeObject(ObjectOutputStream out) throws IOException {
        out.defaultWriteObject();
    }

}
