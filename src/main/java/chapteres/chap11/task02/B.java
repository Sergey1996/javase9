package chapteres.chap11.task02;

import java.io.Serializable;

public class B implements Serializable {
    int i;
    String str;

    public B(int i, String str) {
        this.i = i;
        this.str = str;
    }
}
