package chapteres.chap11.task02;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Application {
    public static void main(String[] args) throws IOException {

        A a = new A(10,"I am A and I serializable");
        B b = new B(10,"I am B and I not serializable");

        serializ(a, Paths.get("fileA"));
        serializ(b, Paths.get("fileB"));

        try(ObjectInputStream in = new ObjectInputStream(Files.newInputStream(Paths.get("fileA")))) {

            A serializA = (A) in.readObject();

            System.out.println(serializA.name);

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }


    }

    private static void serializ(Object object, Path path) throws IOException {
        Annotation serializable = object.getClass().getAnnotation(chapteres.chap11.task02.Serializable.class);
        if(serializable == null) {
            System.out.println(object.toString() + " - Is not Serializable");
        } else {
            ObjectOutputStream out = new ObjectOutputStream(Files.newOutputStream(path));
            out.writeObject(object);
            out.close();
        }
    }
}
