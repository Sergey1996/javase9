package chapteres.chap11.task06;

import java.lang.annotation.Repeatable;

@Repeatable(Todos.class)
public @interface Todo {
    String todo();
}
