package chapteres.chap11.task06;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.AnnotatedConstruct;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.Set;

@SupportedAnnotationTypes("chapteres.chap11.task06.Todos")
@SupportedSourceVersion(SourceVersion.RELEASE_8)
public class TodoProcessor extends AbstractProcessor {
    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        if(annotations.size() == 0) return true;
        File file = new File("Todo2");
        try (FileWriter fileWriter = new FileWriter(file)) {
            for(Element element : roundEnv.getElementsAnnotatedWith(Todos.class)) {
                fileWriter.write(element.getSimpleName().toString() + " : ");
                for(Todo todo : element.getAnnotation(Todos.class).value()) {
                    fileWriter.write(todo.todo() + ", ");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }
}
