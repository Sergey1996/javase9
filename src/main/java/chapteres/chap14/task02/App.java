package chapteres.chap14.task02;


import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class App {
    public static void main(String[] args) throws ScriptException {
        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("nashorn");
        JSON json = ((Invocable) engine).getInterface(engine.eval("JSON"), JSON.class);
        Object result = json.parse("{\"name\": \"Fred\", \"age\": 42}");
        result = json.stringify(result);
        System.out.println(result);
    }

    public interface JSON {
        Object parse(String str);
        String stringify(Object obj);
    }
}
