package chapteres.chap14.task03;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class App {

    private static void bubbleSortJava(int[] a) {
        for (int i = a.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (a[j] > a[j + 1]) {
                    int cur = a[j];
                    a[j] = a[j + 1];
                    a[j + 1] = cur;
                }
            }
        }
    }

    public static void main(String[] args) throws IOException {
        int[] mass = {7, 9, 4, 9, 34, 5, 3};
        int[] mass2 = {7, 9, 4, 9, 34, 5, 3};
        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("nashorn");
        String f = new String(Files.readAllBytes(Paths.get("./src/main/java/chapteres/chap14/task03/myJs.js")));
        try {
            engine.eval(f);
            Invocable inv = (Invocable) engine;
            long time1 = System.currentTimeMillis();
            inv.invokeFunction("bubbleSort", mass);
            long time2 = System.currentTimeMillis();
            System.out.println("Time run of JavaScript: " + (time2 - time1) + " ms");
            System.out.println("Result" + Arrays.toString(mass));
        } catch (ScriptException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        long time12 = System.currentTimeMillis();
        bubbleSortJava(mass2);
        long time22 = System.currentTimeMillis();
        System.out.println("Time run of Java: " + (time22 - time12) + " ms");
        System.out.println("Result" + Arrays.toString(mass2));
    }
}
