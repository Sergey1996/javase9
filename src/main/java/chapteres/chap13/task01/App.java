package chapteres.chap13.task01;

import java.text.NumberFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

public class App {
    public static void main(String[] args) {

        LocalDateTime date = LocalDateTime.now();

        DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT)
                .withLocale(Locale.forLanguageTag("fr-CH"));

        System.out.println(formatter.withLocale(
                Locale.FRENCH).format(date));

        formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT)
                .withLocale(Locale.forLanguageTag("zh-TW"));
        System.out.println(formatter.withLocale(
                Locale.CHINA).format(date));

        formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT)
                .withLocale(Locale.forLanguageTag("th-TH"));
        System.out.println(formatter.withLocale(
                Locale.TAIWAN).format(date));

    }
}
