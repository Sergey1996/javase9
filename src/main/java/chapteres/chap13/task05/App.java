package chapteres.chap13.task05;

import java.text.NumberFormat;
import java.util.Locale;
import java.util.stream.Stream;

public class App {
    public static void main(String[] args) {
        Object [] objects = Stream.of(Locale.getAvailableLocales()).peek((locale)->
                System.out.println(NumberFormat.getCurrencyInstance(locale).format(100.45))).toArray();
    }
}
