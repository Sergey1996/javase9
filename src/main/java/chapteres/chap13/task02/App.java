package chapteres.chap13.task02;

import java.text.NumberFormat;
import java.text.ParseException;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Arrays;
import java.util.Locale;
import java.util.stream.Stream;

public class App {
    public static void main(String[] args) {
        /*Locale[] locales = Locale.getAvailableLocales();
        System.out.println(Arrays.toString(locales));*/
        Object[] objects = Stream.of(Locale.getAvailableLocales()).peek((locale) -> {
            try {
                System.out.println(NumberFormat
                        .getNumberInstance(locale).parse("12.56").doubleValue());
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }).toArray();
    }
}
