package chapteres.chap13.task08;

import java.text.Normalizer;

public class App {
    public static void main(String[] args) {
        String msg = "San Jose\u0301";
        System.out.println(msg);
        String norm = Normalizer.normalize(msg,Normalizer.Form.NFC);
        System.out.println(norm);
    }
}
