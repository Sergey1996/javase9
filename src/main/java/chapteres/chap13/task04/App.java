package chapteres.chap13.task04;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;
import java.util.Scanner;
import java.util.stream.Stream;

public class App {
    public static void main(String[] args) {
        Object [] objects = Stream.of(Locale.getAvailableLocales()).peek((locale)->
                System.out.println(locale.getDisplayName(locale))).toArray();
    }
}
