package chapteres.chap13.task03;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;
import java.util.stream.Stream;

public class App {
    public static void main(String[] args) {
        Object[] objects = Stream.of(Locale.getAvailableLocales()).peek((locale)-> System.out.println(DateTimeFormatter
                .ofLocalizedDate(FormatStyle.SHORT).withLocale(locale)
                .format(LocalDate.now()) + "   " + locale.getDisplayName())).toArray();
    }
}
