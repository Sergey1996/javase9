package chapteres.chap01;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class ChapOne {
    public static void task1(){
        System.setProperty("console.encoding","Cp866");
        System.out.print("Введите целое число: ");
        Scanner scanner = new Scanner(System.in);
        int val = scanner.nextInt();
        System.out.println("Двоичное число: "+Integer.toString(val,2));
        System.out.println("Восмеричное число: "+Integer.toString(val,8));
        System.out.println("Шестнадцатиричное число: "+Integer.toString(val,16));
        if(val!=0) {
            System.out.println("Обратное число с плавающей точкой: " + (double)1/val);
        }else{
            System.out.println(0);
        }
    }

    public static void task3(){
        System.setProperty("console.encoding","Cp866");
        System.out.println("Введите три числа ");
        Scanner scanner = new Scanner((System.in));
        int x = scanner.nextInt();
        int y = scanner.nextInt();
        int z = scanner.nextInt();
        System.out.println("Наибольшее число: ");
        if(x > y && x > z){
            System.out.println(x);
        }else if(y > x && y > z){
            System.out.println(y);
        }else if(z > x && z > y){
            System.out.println(z);
        }else if(x==y && x==z){
            System.out.println(x);
        }else if (x == y){
            if (x>z) System.out.println(x);
            else System.out.println(z);
        }else if (x == z){
            if(x > y) System.out.println(x);
            else System.out.println(y);
        }else if(z == y){
            if(z > x) System.out.println(z);
            else System.out.println(x);
        }
        System.out.println("Matn.max:  " + Math.max(x,Math.max(y,z)));
    }

    public static void task4(){
        System.setProperty("console.encoding","Cp866");
        System.out.println("Минимальное положительное число типа double: " + (double)Math.nextUp(0));
        System.out.println("Максмальное положительное число типа double: " + Double.MAX_VALUE);
    }

    public static void task5(){
        System.out.println((int)Double.MAX_VALUE);
        System.out.println(Double.MAX_VALUE);
        System.out.println(Integer.MAX_VALUE);
    }

    public static void task6(){
        System.setProperty("console.encoding","Cp866");
        System.out.println("Введите число для вычисления факториала: ");
        Scanner scanner = new Scanner(System.in);
        int val = scanner.nextInt();
        BigInteger rez = BigInteger.valueOf(1);
        for(int i=1; i<=val; i++){
            rez=BigInteger.valueOf(i).multiply(rez);
        }
        System.out.println(rez);
    }

    public static void task8(){
        System.setProperty("console.encoding","Cp866");
        System.out.println("Введите строку:");
        Scanner scanner = new Scanner(System.in);
        String str =scanner.nextLine();
        String [] parsStr=str.split("\\s+");
        for(String rez : parsStr){
            System.out.println(rez);
        }
    }

    public static void task10(){
        System.setProperty("console.encoding","Cp866");
        Random random = new Random();
        long val=random.nextLong();
        System.out.println("Случайное число: "+val+"  по основанию 36:  "+Long.toString(val,36));
    }

    public static void task11(){
        System.setProperty("console.encoding","Cp866");
        Scanner scanner = new Scanner(System.in);
        System.out.println("Ведите строку:");
        String str = scanner.nextLine();
        for(int i=0; i<str.length(); i++) {
            System.out.println((int)str.charAt(i));
        }
    }

    public static void task14(){
        System.setProperty("console.encoding","Cp866");
         System.out.println("Введите матрицу: ");
         Scanner scanner = new Scanner(System.in);
         ArrayList<ArrayList<Integer>> matrix = new ArrayList<>();
         String str = scanner.nextLine();
         int y = 0;
         while(y<str.split(" ").length-1) {
             String[] massStr = str.split(" ");
             ArrayList<Integer> line = new ArrayList<>();
             for(int i = 0; i < massStr.length; i++){
                 line.add(Integer.parseInt(massStr[i]));
             }
             matrix.add(line);
             str = scanner.nextLine();
             y++;
         }
         int  sum = 0;
         int [] masSum = new int [matrix.size()];
         for(int i = 0; i < matrix.size(); i++){
             sum = 0;
             for(int j = 0; j < matrix.size(); j++){
                 sum += matrix.get(i).get(j);
             }
             masSum[i] = sum;
         }
         boolean flag = true;
         for(int i = 0; i < masSum.length; i++){
             for(int j = 0; j < masSum.length; j++){
                 if(masSum[i]!=masSum[j]){
                     flag = false;
                     break;
                 }
             }
         }
         if(flag == true){
             for(int i = 0; i < matrix.size(); i++){
                 sum = 0;
                 for(int j = 0; j < matrix.size(); j++){
                     sum += matrix.get(j).get(i);
                 }
                 masSum[i] = sum;
             }
             for(int i = 0; i < masSum.length; i++){
                 for(int j = 0; j < masSum.length; j++){
                     if(masSum[i]!=masSum[j]){
                         flag = false;
                         break;
                     }
                 }
             }
             if(flag == true){
                 int sumDiag1 = 0;
                 int sumDiag2 = 0;
                 int k = matrix.size()-1;
                 for(int i = 0; i < matrix.size(); i++){
                     for(int j = 0; j < matrix.size(); j++){
                         if(i==j) {
                             sumDiag1 += matrix.get(i).get(j);
                             sumDiag2 +=matrix.get(i).get(k);
                         }
                     }
                     k --;
                 }
                 if(flag == true) System.out.println("В матрице содержется магический квадрат");
                 else System.out.println("В матрице не содержится магический квадрат");
             }else System.out.println("В матрице не содержится магический квадрат");
         }else System.out.println("В матрице не содержится магический квадрат");
    }

    public static void task15(){
        System.setProperty("console.encoding","Cp866");
        System.out.println("Введите размер треугольника паскаля ");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        ArrayList<ArrayList<Integer>> trianglePaskal = new ArrayList<>();
        for(int i = 0; i < n; i++){
            ArrayList<Integer> line = new ArrayList<>();
            line.add(1);
            for(int j = 1; j <= i; j++){
                if(i == j){
                    line.add(1);
                }
                else line.add(trianglePaskal.get(i - 1).get(j - 1) + trianglePaskal.get(i - 1).get(j));

            }
            trianglePaskal.add(line);
        }
        System.out.println(trianglePaskal);
    }

    public static double average(double v1, double... values){
        double sum = v1;
        for(double v : values) sum += v;
        return values.length == 0 ? 0 : sum/values.length;
    }

    public static void getTasksChapOne(){
        System.setProperty("console.encoding","Cp866");
        String flag = " ";
        while (flag.equals("q") == false) {
            System.out.println("Введите номер задания: ");
            Scanner scanner = new Scanner(System.in);
            flag = scanner.next();
            switch (flag) {
                case "1":
                    ChapOne.task1();
                    break;
                case "3":
                    ChapOne.task3();
                    break;
                case "4":
                    ChapOne.task4();
                    break;
                case "5":
                    ChapOne.task5();
                    break;
                case "6":
                    ChapOne.task6();
                    break;
                case "8":
                    ChapOne.task8();
                    break;
                case "10":
                    ChapOne.task10();
                    break;
                case "11":
                    ChapOne.task11();
                    break;
                case "14":
                    ChapOne.task14();
                    break;
                case "15":
                    ChapOne.task15();
                    break;
            }
        }
    }
    public static void main(String [] args){
        System.setProperty("console.encoding","Cp866");
        ChapOne.getTasksChapOne();
        ChapOne.average(10);
    }
}
