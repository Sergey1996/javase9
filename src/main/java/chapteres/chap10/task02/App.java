package chapteres.chap10.task02;

import java.util.Arrays;
import java.util.Random;
import java.util.function.Supplier;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class App {
    public static void main(String[] args) {
        int n = 1000000;
        int [] mass = IntStream.generate(()-> new Random().nextInt()).limit(n).toArray();

        Long start = System.currentTimeMillis();
        Arrays.sort(mass);
        Long finish = System.currentTimeMillis();

        System.out.println(finish - start);

        mass = IntStream.generate(()-> new Random().nextInt()).limit(n).toArray();

        Long start1 = System.currentTimeMillis();
        Arrays.parallelSort(mass);
        Long finish1 = System.currentTimeMillis();

        System.out.println(finish1-start1);

    }
}
