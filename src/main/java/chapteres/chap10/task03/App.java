package chapteres.chap10.task03;

import java.io.*;
import java.nio.channels.ClosedByInterruptException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class App {

    public static Runnable getTask(Path path, String word) {
        return new Runnable() {
            @Override
            public void run() {
                try {
                    if(Thread.currentThread().isInterrupted()) {
                        System.out.println("I'm stopped");
                    } else {
                        Thread.currentThread().sleep(500);
                        if (Files.lines(path).anyMatch(w -> w.equals(word))) {
                            //Thread.currentThread().sleep(500);
                            if(Thread.currentThread().isInterrupted()) {
                                System.out.println("I'm stopped");
                            }
                            Thread.currentThread().getThreadGroup().interrupt();
                            System.out.println("Hello, I'm found " + word + " in " + path.toUri());
                        }
                        else {
                            System.out.println("I m not file");
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
    }

    public static void main(String[] args) throws IOException, InterruptedException {

        String word = "Como";
        List<Runnable> runnableList = new ArrayList<>();

        for(Path path : Files.walk(Paths.get("C:/Users/test_javaSE9")).collect(Collectors.toList())) {
            if(path.toFile().isFile()) {
                runnableList.add(getTask(path,word));
            }
        }

        ThreadGroup threadGroup = new ThreadGroup("My thread group");

        for(Runnable runnable : runnableList) {
            Thread thread = new Thread(threadGroup,runnable);
            thread.start();
            thread.sleep(10);
        }
    }
}
