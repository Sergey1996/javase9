package chapteres.chap10.task03;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AppCallable {

    private static String word = " as";

    private static Callable<Path> getRun(Path path) {
        return new Callable<Path>() {
            @Override
            public Path call() throws Exception {
                if(Files.lines(path).anyMatch(w -> w.equals(word))) {
                    return path;
                }
                else throw new Exception();
            }
        };
    }

    public static void main(String[] args) throws IOException, ExecutionException, InterruptedException {

        Stream<Path> pathStream = Files.walk(Paths.get("D:/Users/������/Projects"));
        List<Callable<Path>> callableList = new ArrayList<>();

        for(Path path : pathStream.collect(Collectors.toList())) {
            if(path.toFile().isFile()) {
                callableList.add(getRun(path));
            }
        }

        ExecutorService executorService = Executors.newCachedThreadPool();
        Path path = executorService.invokeAny(callableList);

        System.out.println(path.toUri());
        executorService.shutdown();
    }
}
