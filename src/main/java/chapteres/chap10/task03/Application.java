package chapteres.chap10.task03;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import java.util.stream.Collectors;

public class Application {
    public static void main(String[] args) throws Exception {
        Path directory = Paths.get("D:/Users/������/documents");
        //Path file = Paths.get("/Users/Shared/JavaBasic/Callable/");
        List<Path> pathList = Files.walk(directory)
                .skip(1)
                .collect(Collectors.toList());

        List<Callable<Path>> callableList = new ArrayList<>();
        pathList.stream()
                .forEach(x -> {
                    try {
                        callableList.add(giveMeTask(x));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
        ExecutorService executor = Executors.newCachedThreadPool();

        Path path = executor.invokeAny(callableList);
        System.out.println(path);
        executor.shutdown();

    }

    public static Callable giveMeTask(Path patchToFile) throws IOException {
        return new Callable() {
            @Override
            public Object call() throws Exception {
                try (InputStream in = Files.newInputStream(patchToFile)) {
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in));
                    boolean containWord = bufferedReader.lines()
                            .anyMatch(x -> "a".equals(x));
                    if (containWord) return patchToFile;
                }
                throw new Exception();
            }
        };
    }
}
