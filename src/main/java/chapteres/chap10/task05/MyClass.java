package chapteres.chap10.task05;

public final class MyClass {
    public final int i;
    public MyClass(int i){
        this.i = i;
    }

    public MyClass setI(int i){
        return new MyClass(this.i + i);
    }

}
