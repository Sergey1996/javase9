package chapteres.chap10.task08;

import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

public class App {
    public static Random random = new Random();

    public static void main(String[] args) {
        ConcurrentHashMap<String, Long> concurrentHashMap = new ConcurrentHashMap<>();
        concurrentHashMap.put("a",random.nextLong());
        concurrentHashMap.put("q",random.nextLong());
        concurrentHashMap.put("w",random.nextLong());
        concurrentHashMap.put("e",random.nextLong());
        concurrentHashMap.put("r",random.nextLong());
        concurrentHashMap.put("t",random.nextLong());
        concurrentHashMap.put("y",random.nextLong());
        concurrentHashMap.put("u",random.nextLong());
        concurrentHashMap.put("i",random.nextLong());
        concurrentHashMap.put("o",random.nextLong());
        concurrentHashMap.put("p",random.nextLong());
        concurrentHashMap.put("s",random.nextLong());
        concurrentHashMap.entrySet().forEach(x -> System.out.println(x.getKey() + " : " + x.getValue()));

        Map.Entry<String,Long> q = concurrentHashMap.reduceEntries(1,(e, e1) -> e.getValue() > e1.getValue() ? e : e1);
        System.out.println("------");
        System.out.println(q.getKey() + " : " + q.getValue());


    }

}
