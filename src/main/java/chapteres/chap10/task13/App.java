package chapteres.chap10.task13;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Scanner;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.stream.Stream;

public class App {

    private String word = "a";
    private static BlockingQueue<Path> blockingQueue = new LinkedBlockingQueue<>();
    private  static  BlockingQueue<HashMap<String, Integer>> frequency = new LinkedBlockingQueue<>();

    public static void main(String[] args) throws FileNotFoundException {
        File file = new File("reader");
        Scanner scan = new Scanner(file);
        while (scan.hasNext()) {
            System.out.println(scan.next());
        }
    }

}
