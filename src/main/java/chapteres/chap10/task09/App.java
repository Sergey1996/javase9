package chapteres.chap10.task09;

import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.LongAdder;

public class App {



    public static void main(String[] args) {
        AtomicLong atomicLong = new AtomicLong();
        LongAdder longAdder = new LongAdder();

        Long one = System.currentTimeMillis();

        for(int i = 0; i < 1000; i++) {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    for(int i = 0; i < 1000000; i++) {
                        //atomicLong.incrementAndGet();
                        longAdder.increment();
                    }
                }
            });

            thread.start();
        }

        while(longAdder.longValue() < 1000000000) {}
        System.out.println(longAdder.longValue());
        Long two = System.currentTimeMillis();

        System.out.println("Adder : " + (two - one));

    }
}
