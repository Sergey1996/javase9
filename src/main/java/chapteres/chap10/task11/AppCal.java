package chapteres.chap10.task11;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.*;
import java.util.stream.Collectors;

public class AppCal {

    private static String word = "a";

    public static void main(String[] args) throws ExecutionException, InterruptedException, IOException {

        Callable<BlockingQueue<Path>> callable = new Callable<BlockingQueue<Path>>() {
            @Override
            public BlockingQueue<Path> call() throws Exception {
                BlockingQueue<Path> queue = new ArrayBlockingQueue<>(10);
                for(Path path : Files.walk(Paths.get("D:/Users/������/Projects")).collect(Collectors.toList())) {
                    if(path.toFile().isFile()) {
                        queue.put(path);
                    }
                }
                return queue;
            }
        };

        ExecutorService executor = Executors.newCachedThreadPool();
        Future<BlockingQueue<Path>> queueFuture = executor.submit(callable);

        System.out.println(queueFuture.get().size());
        /*Thread thread1 = new Thread(getRun(queueFuture.get()));
        thread1.run();*/


    }

    private static Runnable getRun(BlockingQueue<Path> blockingQueue) throws InterruptedException, IOException {
        return new Runnable() {
            @Override
            public void run() {
                Path path = null;
                try {
                    path = blockingQueue.take();
                    if(Files.lines(path).anyMatch(w -> w.equals(word))) {
                        System.out.println(path.toUri());
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
    }
}
