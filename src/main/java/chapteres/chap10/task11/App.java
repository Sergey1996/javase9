package chapteres.chap10.task11;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class App {

   private static BlockingQueue<Path> blockingQueue = new ArrayBlockingQueue<Path>(10);

   private static String word = "a";

    public static void main(String[] args) throws IOException {

        Stream<Path> stream = Files.walk(Paths.get("D:/Users/������/Projects"));

        Runnable run1 = new Runnable() {
            @Override
            public void run() {
                for(Path path : stream.collect(Collectors.toList())) {
                    if(path.toFile().isFile()) {
                        blockingQueue.add(path);
                    }
                }
                Thread.currentThread().interrupt();
            }
        };

        Thread thread1 = new Thread(run1);
        thread1.run();
        System.out.println(blockingQueue.size());


        /*Thread thread2 = new Thread(getRun());
        thread2.run();
*/
        /*Thread thread3 = new Thread(getRun());
        thread3.run();

        Thread thread4 = new Thread(getRun());
        thread4.run();*/

        }

        private static Runnable getRun() {
        return new Runnable() {
            @Override
            public void run() {
                try {
                    Path path = blockingQueue.take();
                    if(Files.lines(path).anyMatch(w -> w.equals(word))) {
                        System.out.println("Hello, I found " + word + " in " + path.toUri());
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Thread.currentThread().interrupt();
            }
        };
        }

}
