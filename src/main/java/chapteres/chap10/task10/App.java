package chapteres.chap10.task10;

import java.util.Random;
import java.util.concurrent.atomic.LongAccumulator;

public class App {

    public static LongAccumulator max = new LongAccumulator(Long::max,0);
    public static LongAccumulator min = new LongAccumulator(Long::min,0);

    public static Random random = new Random();

    public static void main(String[] args) {
        for(int i = 0; i < 50; i++){
            long val = random.nextLong();
            max.accumulate(val);
            min.accumulate(val);
        }

        System.out.println("Max :  " + max.get());
        System.out.println("Min :  " + min.get());

    }

}
