package chapteres.chap10.task01;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class App {

    private static AtomicInteger nextNumber = new AtomicInteger();

    public static void main(String[] args) throws IOException {

        String word = "Como";
        List<Path> listPath = new ArrayList<>();

         Stream<Path> stream = Files.walk(Paths.get("C:/Users/test_javaSE9"));
         for(Path path : stream.collect(Collectors.toList())) {
             if(path.toFile().isFile()) {
                 Runnable task = () -> {
                     try {
                         if (Files.lines(path).anyMatch(s -> s.equals(word))) {
                             int i = nextNumber.incrementAndGet();
                             if(i==1)
                             listPath.add(path);
                         }
                     } catch (IOException e) {
                         e.printStackTrace();
                     }
                 };
                 Thread thread = new Thread(task);
                 thread.run();
             }
         }
        for(Path file : listPath) {
            System.out.println(file.toUri().toString());
        }

    }
}
