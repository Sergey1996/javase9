package chapteres.chap10.task06;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class App {
    public static void main(String[] args) throws IOException {
        List<File> fileList = Files.walk(Paths.get("C:/Users/test_javaSE9"))
                .filter(f -> f.toFile().isFile())
                .map(x -> new File(x.toString()))
                .collect(Collectors.toList());

        ConcurrentHashMap<String, Set<File>> concurrentHashMap = new ConcurrentHashMap<>();

        fileList.parallelStream().map(x -> {
                    try (InputStream in = Files.newInputStream(x.toPath())) {
                        Scanner scanner = new Scanner(in);
                        while (scanner.hasNextLine()){
                            String nextLine = scanner.nextLine();
                            /*if(concurrentHashMap.get(nextLine) == null){
                                concurrentHashMap.put(nextLine,new HashSet<>());
                                concurrentHashMap.get(nextLine).add(x);
                            }else {
                                concurrentHashMap.get(nextLine).add(x);
                            }*/
                            Set<File> set = new HashSet<>();
                            set.add(x);
                            concurrentHashMap.merge(nextLine,set,(k,v) -> {
                                Set<File> files = k;
                                files.add(v.iterator().next());
                                return files;
                            });
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return true;
                })
                .forEach(x -> System.out.print(""));

        concurrentHashMap.entrySet().forEach(x -> {
            System.out.print(x.getKey() + ": ");
            for(File f: x.getValue()){
                System.out.print(f.getName() + " | ");
            }
            System.out.println("\n" + "---------");
        });

    }
}
