package chapteres.chap07.task12;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class App {
    public static void main(String[] args) throws FileNotFoundException {
        List<String> list = new ArrayList<>();
        Scanner scanner = new Scanner(new File("reader11"));
        while(scanner.hasNext()){
            list.add(scanner.next());
        }

        System.out.println(list);

        String word = list.get(list.size()-1);
        if(word.charAt(word.length()-1) != '.') {
            list.set(list.size() - 1, word + ".");
        }
        word = list.get(0);
        list.set(0,word.replaceFirst(String.valueOf(word.charAt(0)),(String.valueOf(word.charAt(0)).toUpperCase())));

        System.out.println(list);
        Collections.shuffle(list);

        System.out.println(list);

        word = list.get(list.size()-1);
        if(word.charAt(word.length()-1) != '.') {
            list.set(list.size() - 1, word + ".");
        }
        word = list.get(0);
        list.set(0,word.replaceFirst(String.valueOf(word.charAt(0)),(String.valueOf(word.charAt(0)).toUpperCase())));

        System.out.println(list);

    }
}
