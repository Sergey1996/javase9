package chapteres.chap07.task10;

public class Neighbor implements Comparable<Neighbor> {

    private String nameCity;
    private double distance;

    Neighbor(String nameCity, double distance){
        this.nameCity = nameCity;
        this.distance = distance;
    }

    public String getNameCity(){
        return this.nameCity;
    }

    public double getDistance(){
        return this.distance;
    }

    @Override
    public int compareTo(Neighbor o) {
        if(this.distance > o.distance) return -1;
        else if(this.distance < o.distance) return 1;
        else return 0;
    }
}
