package chapteres.chap07.task10;

import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

public class App {

    public static PriorityQueue<Neighbor> deckstr(Map<String, PriorityQueue<Neighbor>> mapCity,
                                                  String cityStart, String cityFinish){
        return null;
    }

    public static void main(String[] args) {

        Map<String, PriorityQueue<Neighbor>> mapCity = new HashMap<>();
        String msc = "Moscow";
        String smr = "Samara";
        String tlt = "Tolity";
        String kzn = "Kazan";
        mapCity.put(smr,new PriorityQueue<>());
        mapCity.get(smr).add(new Neighbor(tlt,86));
        mapCity.get(smr).add(new Neighbor(kzn,350));
        mapCity.put(msc, new PriorityQueue<>());
        mapCity.get(msc).add(new Neighbor(tlt,1200));
        mapCity.get(msc).add(new Neighbor(kzn,1300));
        mapCity.put(tlt, new PriorityQueue<>());
        mapCity.get(tlt).add(new Neighbor(smr,86));
        mapCity.get(tlt).add(new Neighbor(msc,1200));
        mapCity.get(tlt).add(new Neighbor(kzn,190));
        mapCity.put(kzn, new PriorityQueue<>());
        mapCity.get(kzn).add(new Neighbor(smr,350));
        mapCity.get(kzn).add(new Neighbor(msc,1300));
        mapCity.get(kzn).add(new Neighbor(tlt,190));
    }
}
