package chapteres.chap07.task05;

import java.util.*;

public class Main {

    public static void swap(List<?> list, int i, int j){

         swapHelper(list,i,j);
    }

    public static <T> void swapHelper(List<T> list, int i, int j){
            if (list instanceof RandomAccess) {
                T t = list.get(i);
                list.set(i, list.get(j));
                list.set(j, t);
            } else {
                Iterator iterator = list.iterator();
                T t, val = null;
                if (i < j) {
                    for(int k = 0; k <= j; k++){
                        t = (T) iterator.next();
                        if(k==i) {
                            val = t;
                        }
                        if(k==j) {
                            list.set(j,val);
                            list.set(i,t);
                        }
                    }
                } else {
                    for(int k = 0; k <= i; k++){
                        t = (T) iterator.next();
                        if(k==j) {
                            val = t;
                        }
                        if(k==i) {
                            list.set(i,val);
                            list.set(j,t);
                        }
                    }
                }
            }

    }


    public static void main(String[] args) {

        List<Integer> arrayList = new ArrayList<>();
        arrayList.add(5);
        arrayList.add(10);
        swap(arrayList,0,1);
        System.out.println(arrayList);

        List<Integer> linkedList = new LinkedList<>();
        linkedList.add(5);
        linkedList.add(10);
        swap(linkedList,0,1);
        System.out.println(linkedList);
    }

}
