package chapteres.chap07.task08;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class task08 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(new File("reader"));
        Set<String> mySet = new HashSet<>();
        while (scanner.hasNext()){
            mySet.add(scanner.nextLine());
        }
        System.out.println(mySet);
    }
}
