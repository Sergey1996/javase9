package chapteres.chap07.task09;

import java.util.HashMap;
import java.util.Map;

public class App {
    public static void main(String[] args) {
        Map<String,Integer> myMap = new HashMap<>();
        myMap.put("Alice",1);
        myMap.put("Alice",2);
        System.out.println(myMap.get("Alice"));

        myMap.merge("Alice",1,Integer::sum);
        System.out.println(myMap.get("Alice"));

        if(myMap.containsKey("Alice")){
            myMap.replace("Alice",myMap.getOrDefault("Alice",1) + 1);
        }else{
            myMap.putIfAbsent("Alice",1);
        }
        System.out.println(myMap.get("Alice"));

        if(myMap.get("Alice") != null){
            myMap.put("Alice",myMap.get("Alice")+1);
        }
        else{
            myMap.put("Alice",1);
        }
        System.out.println(myMap.get("Alice"));

        if(1 != myMap.getOrDefault("Alice",1)){
            myMap.put("Alice",myMap.get("Alice")+1);
        }
        else{
            myMap.put("Alice",1);
        }
        System.out.println(myMap.get("Alice"));

        if(myMap.putIfAbsent("Alice",1) != null){
            myMap.put("Alice",myMap.get("Alice")+1);
        }
        System.out.println(myMap.get("Alice"));


    }
}
