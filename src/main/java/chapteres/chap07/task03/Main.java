package chapteres.chap07.task03;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class Main {

    private static Set<Object> join(Set<Object> set1,Set<Object> set2){
        Set<Object> rez = new HashSet<>();
        rez.addAll(set1);
        rez.addAll(set2);
        return rez;
    }

    private static Set<Object> intersection(Set<Object> set1,Set<Object> set2){
        Set<Object> rez = new HashSet<>();
        rez.addAll(set1);
        rez.retainAll(set2);
        return rez;
    }

    private static Set<Object> difference(Set<Object> set1,Set<Object> set2){
        Set<Object> rez = new HashSet<>();
        rez.addAll(set1);
        rez.removeAll(set2);
        return rez;
    }

    public static void main(String[] args) {
        Set<Object> set1 = new HashSet<>();
        Set<Object> set2 = new HashSet<>();
    }
}
