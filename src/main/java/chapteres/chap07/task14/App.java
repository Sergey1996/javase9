package chapteres.chap07.task14;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class App {

    public static Set<Integer> method(int n){
        return Stream.iterate(0, x -> x + 1).limit(n).collect(Collectors.toSet());
    }

    public static void main(String[] args) {
        Set <Integer> set = method(10);
        System.out.println(set);
    }
}
