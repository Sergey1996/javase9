package chapteres.chap07.task02;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class Main {

    private static List<String> metod1(List<String> masStr){
        Iterator iterator = masStr.iterator();
        while (iterator.hasNext()){
            String cur = (String) iterator.next();
            masStr.set(masStr.indexOf(cur),cur.toUpperCase());
        }
        return masStr;
    }

    public static List<String> metod2(List<String> masStr) {
        for(int i = 0; i < masStr.size(); i++){
            masStr.set(i,masStr.get(i).toUpperCase());
        }
        return masStr;
    }

    public static List<String> metod3(List<String> masStr){
        masStr.replaceAll(str -> str.toUpperCase());
        return masStr;
    }

    public static void main(String[] args) {

        List<String> masStr = new ArrayList<>();
        masStr.add("java");
        masStr.add("se9");
        System.out.println(metod3(masStr));
    }
}
