package chapteres.chap07.task01;

import java.util.*;

public class Main {

    public static Set<Integer> metod(int n){
        Set<Integer> mySet = new HashSet<>();

        for(int i = 2; i <= n; i++){
            mySet.add(i);
        }

        int s;
        for(int i = 2; i <= n; i++){
            s = i*i;
            mySet.remove(s);
            s=i;
            for(int j = 1; j < mySet.size(); j++){
                s = s*(s + j);
                mySet.remove(s);
                s = i;
            }
        }

        return mySet;
    }

    public static BitSet getBitSet(int n){
        BitSet myBitSet = new BitSet();

        for(int i = 2; i <= n; i++){
            myBitSet.set(i);
        }

        int s;
        for(int i = 2; i <= n; i++){
            s = i*i;
            myBitSet.clear(s);
            s=i;
            for(int j = 1; j < myBitSet.size(); j++){
                s = s*(s + j);
                myBitSet.clear(s);
                s = i;
            }
        }

        return myBitSet;
    }

    public static void main(String [] args){

        System.out.println(metod(30));
        System.out.println(getBitSet(30));

    }
}
