package chapteres.chap07.task07;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class App {
    public static void main(String[] args) throws FileNotFoundException {
        Map<String,Integer> myMap = new HashMap<>();
        File myFile = new File("reader");
        Scanner scanner = new Scanner(myFile);
        String str = "";
        while (scanner.hasNext()){
            str = scanner.next();
            if(myMap.containsKey(str)){
               myMap.put(str,myMap.get(str)+1);
            }
            else {
                myMap.put(str, 1);
            }
        }
        System.out.println(myMap);
    }
}
