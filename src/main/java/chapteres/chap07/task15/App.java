package chapteres.chap07.task15;

import java.util.HashSet;
import java.util.function.IntFunction;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class App<R>  implements IntFunction<R> {

    @Override
    public R apply(int value) {
        return (R) Stream.iterate(0, x -> x + 1).limit(value).collect(Collectors.toSet());
    }
}
