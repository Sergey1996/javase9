package chapteres.chap07.task13;

import java.util.LinkedHashMap;
import java.util.Map;

public class Cache<K,V> extends LinkedHashMap<K,V> {

    private int MAX_SIZE;

    public Cache(int size){
        super();
        this.MAX_SIZE = size;
    }

    @Override
    protected boolean removeEldestEntry(Map.Entry<K,V> eldest) {
        return size() > MAX_SIZE;
    }

}
