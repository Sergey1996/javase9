package chapteres.chap07.task13;

public class App {
    public static void main(String[] args) {
        Cache<Integer, String> cache = new Cache<>(3);
        for(int i = 0; i < 5; i++){
            cache.put(i,"obj" + i);
        }
        System.out.println(cache);
    }
}
