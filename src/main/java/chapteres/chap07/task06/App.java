package chapteres.chap07.task06;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class App {
    public static void main(String[] args) {
        Map<String, Set<Integer>> map = new TreeMap<>();
        Set<Integer> set = new HashSet<>();
        Integer val = new Integer(50000);
        set.add(val);
        map.put("MyZp",set);
        System.out.println(map.get("MyZp"));
    }
}
