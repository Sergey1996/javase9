package chapteres.chap07.task11;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class App {
    public static void main(String[] args) throws FileNotFoundException, InterruptedException {
        List<String> list = new ArrayList<>();
        Scanner scanner = new Scanner(new File("reader11"));
        while(scanner.hasNext()){
            list.add(scanner.next());
        }
        System.out.println(list);

        String first,last;
        first = list.get(0);
        last = list.get(list.size()-1);

        Collections.shuffle(list);
        System.out.println(list);

        chapteres.chap07.task05.Main.swap(list,0,list.indexOf(first));
        chapteres.chap07.task05.Main.swap(list,list.size()-1,list.indexOf(last));
        System.out.println(list);
    }
}
