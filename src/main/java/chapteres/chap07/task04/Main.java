package chapteres.chap07.task04;

import java.util.ArrayList;
import java.util.Iterator;

public class Main {

    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>();
        list.add("Object1");
        list.add("Object2");
        list.add("Object3");
        list.add("Object4");
        list.add("Object2");
        list.add("Object5");
        Iterator<String> iterator = list.iterator();

        while (iterator.hasNext()){
            String str = iterator.next();
            if(str.equals("Object2")){
                list.remove(str);
                //iterator.remove();
            }
        }
    }
}
