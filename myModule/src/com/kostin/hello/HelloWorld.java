package com.kostin.hello;

import com.horstmann.greetsvc.GreeterService;
import com.horstmann.greetsvc.internal.GermanGreeterFactory;

import java.util.ServiceLoader;

public class HelloWorld {
    public static void main(String[] args) {
       // ServiceLoader<GreeterService> services = ServiceLoader.load(GreeterService.class);
        GreeterService greeterService = GermanGreeterFactory.provider();
        System.out.println(greeterService.getLocale().getDisplayName());
    }
}
