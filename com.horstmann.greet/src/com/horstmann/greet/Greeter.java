package com.horstmann.greet;


public interface Greeter {
    System.Logger LOG = System.getLogger("myLogger");

    static Greeter newInstance() {
        LOG.log(System.Logger.Level.ERROR,"err");
        return new com.horstmann.greet.internal.GreeterImpl();
    }

    String greet(String subject);
}
