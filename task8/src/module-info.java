module task8 {
    exports com.horstmann.greetsvc;
    exports com.horstmann.greetsvc.internal;
    provides com.horstmann.greetsvc.GreeterService
            with com.horstmann.greetsvc.internal.FrenchGreeter,
                    com.horstmann.greetsvc.internal.GermanGreeterFactory;
}